#lang racket

(require rackunit)
(require "extras.rkt")
(require 2htdp/universe)   
(require 2htdp/image)
(require "StatefulWorld.rkt")
(require "sets.rkt")


(provide
 make-world
 world-after-tick
 world-after-key-event
 world-after-mouse-event
 run-world
 run
 make-stealth-goofball
 make-round-goofball
 make-DAGR
 make-SAGR
 SimulatorState<%>
 Goofball<%>
 Robot<%>)



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; CONSTANTS:


(define CANVAS-WIDTH 800)
(define CANVAS-HEIGHT 600)
(define EMPTY-CANVAS (empty-scene CANVAS-WIDTH CANVAS-HEIGHT))
(define ZERO 0)
(define FUEL-LIMIT 400)
(define CHANGE-IN-SPEED 8)
(define LONGEST-DISTANCE 50)
(define SAGR-LENGTH 11)
(define DAGR-LENGTH 15)
(define HALF 0.5)
(define INCREMENT 1)
(define TWICE 2)
(define RIGHT "right")
(define LEFT "left")
(define UP "up")
(define DOWN "down")
(define SPACE " ")
(define NEW-GOOFBALL "n")
(define NEW-SAGR "s")
(define NEW-DAGR "r")
(define ROBOT-Y-OFFSET 3)
(define ROBOT-VY -3)
(define SAGR-OFFSET -10)
(define DAGR-OFFSET -5)
(define LAUNCH-Y-LIMIT 10)
(define Y-UPWARDS "u")
(define Y-DOWNWARDS "d")
(define INITIAL-X 50)
(define INITIAL-Y 75)
(define INITIAL-VX 5)
(define INITIAL-VY 3)
(define ONE 1)
(define TWO 2)
(define INITIAL-LAUNCH-X 800)




;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;; Data Definitions



;; A SimulatorState is a (make-world ListOfGoofball<%> ListOfRobot<%>)
;; INTERP: (make-world log lor) represents a simulatorstate containing the
;; objects in log and lor.


;; A Goofball<%> is either a
;; - (make-stealth-goofball Integer Integer Integer Integer)
;; - (make-round-goofball Integer Integer Integer Integer)
;; INTERP: the four integers represent x, y of center coordinates and vx, vy
;; velocity components respectively


;; A Robot<%> is either a
;; - (make-SAGR Integer Integer Integer Integer)
;; - (make-DAGR Integer Integer Integer Integer)
;; INTERP: the four integers represent x, y of center coordinates and vx, vy
;; velocity components respectively





;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; TESTING FRAMEWORK:

;; simulator-equal? : SimulatorState<%> SimulatorState<%> -> Boolean
;; GIVEN: two simulatorstates
;; RETURNS: true if they have the same parameters
;; STRATEGY: Combine Simpler Functions
(define (simulator-equal? s1 s2)
  (and
    (= 
      (send s1 launcher-x)
      (send s2 launcher-x))
    (= 
      (send s1 launcher-y)
      (send s2 launcher-y))
    (list-equal? 
      (send s1 goofballs)
      (send s2 goofballs) ONE)
    (list-equal? 
      (send s1 robots)
      (send s2 robots) TWO)
    (equal? 
      (send s1 is-paused?)
      (send s2 is-paused?))))



;; list-equal? : ListOfX ListofX -> Boolean
;; GIVEN: two lists of X
;; WHERE: X could be Goofball, Robot or Explosion
;; RETURNS: true if they have the same parameters
;; STRATEGY: Cases based on n
(define (list-equal? a1 a2 n)
  (cond
    [(and (empty? a1) (empty? a2)) true]
    [(= n ONE) (and (goofball-equal? (first a1) (first a2))
                  (list-equal? (rest a1) (rest a2) n))]
    [(= n TWO) (and (robot-equal? (first a1) (first a2))
                  (list-equal? (rest a1) (rest a2) n))]))


;; goofball-equal? : Goofball<%> Goofball<%> -> Boolean
;; GIVEN: two goofballs
;; RETURNS: true if they have the same parameters
(define (goofball-equal? g1 g2)
  (and
   (=
    (send g1 get-x)
    (send g2 get-x))
   (=
    (send g1 get-y)
    (send g2 get-y))
   (=
    (send g1 get-vx)
    (send g2 get-vx))
   (=
    (send g1 get-vy)
    (send g2 get-vy))
   (equal?
    (send g1 stealthy?)
    (send g2 stealthy?))))
   


;; robot-equal? : Robot<%> Robot<%> -> Boolean
;; GIVEN: two robots
;; RETURNS: true if they have the same parameters
(define (robot-equal? r1 r2)
  (and
   (=
    (send r1 get-x)
    (send r2 get-x))
   (=
    (send r1 get-y)
    (send r2 get-y))
   (=
    (send r1 get-vx)
    (send r2 get-vx))
   (=
    (send r1 get-vy)
    (send r2 get-vy))
   (equal?
    (send r1 smart?)
    (send r2 smart?))))



;; explosions-equal? : Explosion<%> Explosion<%> -> Boolean
;; GIVEN: two Explosions
;; RETURNS: true if they have the same parameters
(define (explosions-equal? e1 e2)
  (and
   (=
    (send e1 get-x)
    (send e1 get-x))
   (=
    (send e1 get-y)
    (send e1 get-y))
   (=
    (send e1 get-counter)
    (send e1 get-counter))))




;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;



;;; run : PosNum -> SimulatorState<%> 
;;; GIVEN: a frame rate (in seconds/tick)
;;; RETURNS: the final state of the world after starting and running
;;;     the standard initial world
(define (run rate)
  (big-bang (initial-world)
            (on-tick world-after-tick rate)
            (on-draw world-to-scene)
            (on-key world-after-key-event)
            (on-mouse world-after-mouse-event)))



;;; run-world : PosNum SimulatorState<%> -> SimulatorState<%>
;;; GIVEN: a frame rate (in seconds/tick) and a SimulatorState<%>
;;; WHERE: the SimulatorState<%> was returned by one of these provided
;;;         functions
;;; RETURNS: the final state of the world after running the given world
;;; EFFECT: this function may change the state of its second argument,
;;;     but is not required to do so
(define (run-world rate ss)
  (big-bang ss
            (on-tick world-after-tick rate)
            (on-draw world-to-scene)
            (on-key world-after-key-event)
            (on-mouse world-after-mouse-event)))


;;; initial-world : -> SimulatorState<%>
;;; GIVEN: no arguments
;;; RETURNS: a world with an initial state, without goofballs and robots
(define (initial-world)
  (make-world empty empty))



;;; world-after-tick : SimulatorState<%> -> SimulatorState<%>
;;; GIVEN: a SimulatorState<%>
;;; WHERE: the SimulatorState<%> was returned by one of these provided
;;;        functions
;;; RETURNS: the SimulatorState<%> that should follow the given
;;;        SimulatorState<%>
;;; EFFECT: this function may change the state of its argument,
;;;     but is not required to do so
(define (world-after-tick ss)
  (begin (send ss after-tick) ss))



;;; world-after-key-event : SimulatorState<%> KeyEvent -> SimulatorState<%>
;;; GIVEN: a SimulatorState<%> and a KeyEvent
;;; WHERE: the SimulatorState<%> was returned by one of these provided
;;;        functions
;;; RETURNS: the SimulatorState<%> that should follow the given 
;;;        SimulatorState<%> after the given KeyEvent
;;; EFFECT: this function may change the state of its first argument,
;;;     but is not required to do so
(define (world-after-key-event ss kev)
  (begin (send ss after-key-event kev) ss))
  

 
;;; world-after-mouse-event : SimulatorState<%> Int Int MouseEvent ->
;;;                           SimulatorState<%>
;;; GIVEN: A SimulatorState<%>, the x- and y-coordinates of a mouse event,
;;;     and the mouse event
;;; WHERE: the SimulatorState<%> was returned by one of these provided
;;;        functions
;;; RETURNS: the SimulatorState<%> that should follow the given 
;;;          SimulatorState<%> after the given mouse event
;;; EFFECT: this function may change the state of its first argument,
;;;     but is not required to do so
(define (world-after-mouse-event ss mx my mev)
  (begin (send ss after-mouse-event mx my mev) ss)) 



;;; world-to-scene : SimulatorState<%> -> Scene
;;; GIVEN: a SimulatorState<%> 
;;; WHERE: the SimulatorState<%> was returned by one of these provided
;;;         functions
;;; RETURNS: a Scene that depicts the state of the current SimulatorState
(define (world-to-scene ss)
  (send ss to-scene))



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;; SIMULATORSTATE% CLASS

;; A SimulatorState% is a (make-world ListOfGoofball<%> ListOfRobot<%>)


(define SimulatorState%
  (class* object% (SimulatorState<%>)

    ;;--------------------------------------------------------------------

    (init-field launch-y log lor)
    ;; y: PosInteger, balls: ListOfGoofball, robs: ListOfRobot
    (init-field paused?)
    ;; paused?: Boolean
    (init-field explosions)
    ;; explosions: ListOfExplosion
    
    (init-field [launch-x INITIAL-LAUNCH-X])
    ;; PosInt, default value of launcher-x

    ;;--------------------------------------------------------------------

    (super-new)

    ;;--------------------------------------------------------------------

    ;; launcher-x : -> Integer
    ;; RETURNS: the x coordinate of the launcher
    (define/public (launcher-x)
      launch-x)

    ;; launcher-y : -> Integer
    ;; RETURNS: the y coordinate of the launcher
    (define/public (launcher-y)
      launch-y)

    ;; goofballs : -> ListOfGoofball<%>
    ;; RETURNS: the list of goofball in this SimulatorState
    (define/public (goofballs)
      log)

    ;; robots : -> ListOfRobot<%>
    ;; RETURNS: the list of robot in this SimulatorState
    (define/public (robots)
      lor)

    ;; is-paused? : -> Boolean
    ;; RETURNS: true iff this SimulatorState is paused
    (define/public (is-paused?)
      paused?)

    ;; get-explosions : -> ListOfExplosion<%>
    ;; RETURNS: the list of explosions in this SimulatorState
    (define/public (get-explosions)
      explosions)


    ;;-------------------------------------------------------------------
    ;;-------------------------------------------------------------------
    

    ;; AFTER-TICK ->

    
    ;; after-tick : -> Void
    ;; GIVEN: no arguments
    ;; EFFECT: updates this simulatorstate to its state at the next tick
    ;; STRATEGY: Cases based on paused?
    (define/public (after-tick)
      (local
        ((define new-log (new-goofballs))
         (define new-lor (new-robots))
         (define new-loe (new-explosions)))
        (if paused?
            (new-widgets-while-paused new-log new-lor new-loe)
            (new-widgets-while-unpaused new-log new-lor new-loe))))


    ;; new-goofballs : -> ListOfGoofball<%>
    ;; GIVEN: no arguments
    ;; RETURNS: a list of goofballs that have not coincided, or overlapped with
    ;;          other goofballs and robots
    ;; STRATEGY: Combine Simpler functions
    (define (new-goofballs)
      (append (set-diff (widgets-coincide log)
                        (second (robots-overlap-goofballs)))
              (turn-stealth-to-round (stealth-to-round))))


    ;; new-robots : -> ListOfRobot<%>
    ;; GIVEN: no arguments
    ;; RETURNS: a list of robots that have not coincided, or overlapped with
    ;;          other goofballs and robots
    ;; STRATEGY: Combine Simpler functions
    (define (new-robots)
      (robot-speed-change
       (set-diff (widgets-coincide lor)
                 (first (robots-overlap-goofballs)))))


    ;; new-explosions : -> ListOfExplosion
    ;; GIVEN: no arguments
    ;; RETURNS: a list of explosions obtained after goofballs and robots have 
    ;;           exploded
    ;; STRATEGY: Combine Simpler functions
    (define (new-explosions)
      (initialize-explosions (goofballs-for-explosions)))
   


    ;; new-widgets-while-paused : ListOfGoofball<%> ListOfRobot<%> 
    ;;                          ListOfExplosion<%> -> Void
    ;; GIVEN: an updated list of goofball, robots and explosions
    ;; EFFECT: updates the paused simulatorstate to its state at the next tick
    ;; STRATEGY: Combine Simpler functions
    (define (new-widgets-while-paused balls robs exp)
      (begin
        (return-goofballs
         balls
         (lambda (obj) (send obj after-tick-while-paused)))
        (return-robots
         robs
         (lambda (obj) (send obj after-tick-while-paused)))
        (return-explosions
         exp
         (lambda (obj) (send obj after-tick-while-paused)))))



    ;; new-widgets-while-unpaused : ListOfGoofball<%> ListOfRobot<%> 
    ;;                           ListOfExplosion -> Void
    ;; GIVEN: an updated list of goofball, robots and explosions
    ;; EFFECT: updates the unpaused simulatorstate to its state at the next tick
    ;; STRATEGY: Combine Simpler functions
    (define (new-widgets-while-unpaused balls robs exp)
      (begin
        (return-goofballs
         balls
         (lambda (obj) (send obj after-tick)))
        (return-robots
         robs
         (lambda (obj) (send obj after-tick)))
        (return-explosions
         exp
         (lambda (obj) (send obj after-tick)))))



    ;; return-goofballs : ListOfGoofball<%> (Goofball<%> -> Void) -> Void
    ;; GIVEN: a list of goofballs and a function
    ;; WHERE: the function updates each goofball in the list 
    ;; EFFECT: a list of goofballs updated to its state at the next event
    ;; STRATEGY: Combine Simpler Functions
    (define (return-goofballs balls fn)
      (begin
        (set! log balls)
        (for-each fn log)))


    ;; return-robots : ListOfRobot<%> (Robot<%> -> Void) -> Void
    ;; GIVEN: a list of robots and a function
    ;; WHERE: the function updates each robot in the list 
    ;; EFFECT: a list of robots updated to its state at the next event
    ;; STRATEGY: Combine Simpler Functions
    (define (return-robots robs fn)
      (begin
        (set! lor robs)
        (for-each fn lor)))

    
    ;; return-explosions : ListOfExplosion (Explosion<%> -> Void) -> Void
    ;; GIVEN: a list of explosions and a function
    ;; WHERE: the function updates each explosion in the list 
    ;; EFFECT: a list of explosions updated to its state at the next event
    ;; STRATEGY: Combine Simpler Functions
    (define (return-explosions exp fn)
      (begin
        (set! explosions exp)
        (for-each fn explosions)))


    ;; turn-stealth-to-round : ListOfGoofball<%>(Stealth)
    ;;                         -> ListOfGoofball<%>(Round)
    ;; GIVEN: a list of stealth goofballs
    ;; RETURNS: a list of round goofballs with same attributes as that of
    ;;          given goofballs
    ;; EXAMPLE: 
    ;; (turn-stealth-to-round (list (make-stealth-goofball 500 400 6 7))) 
    ;;    =>   (make-round-goofball 500 400 6 7) 1)
    ;; DESIGN STRATEGY: Use HOF Map on lst
    (define (turn-stealth-to-round lst)
      (map
       (lambda (x)
         (make-round-goofball
          (send x get-x)
          (send x get-y)
          (send x get-vx)
          (send x get-vy)))
       lst))



    ;; initialize-explosions : ListOfGoofball<%> -> ListOfExplosion
    ;; GIVEN: a list of goofballs
    ;; RETURNS: a list of explosions where each explosion consists of x and y
    ;;          coordinate of the given goofball
    ;; EXAMPLE: (initialize-explosions (list (make-stealth-goofball 10 10 2 2)))
    ;;         => (new Explosion% [x 10][y 10][counter 100])
    ;; DESIGN STRATEGY: Use HOF Map on lst
    (define (initialize-explosions lst)
      (map
       (lambda (x)
         (new Explosion% [x (send (first lst) get-x)]
              [y (send (first lst) get-y)] [counter 100]))
       lst))
    
    
    
    
    ;; goofballs-for-explosions : -> ListOfGoofball<%>
    ;; RETURNS: a list of goofballs that have exploded
    ;; DESIGN STRATEGY: Combine Simpler Functions
    (define (goofballs-for-explosions)
      (append (get-coincided-goofballs)
              (second (robots-overlap-goofballs))))
    
    
    ;; get-coincided-goofballs : -> ListOfGoofball<%>
    ;; GIVEN: a list of coincided goofball
    ;; RETURNS: a List of Goofballs that have coincided
    ;; DESIGN STRATEGY: Combine Simpler Functions 
    (define (get-coincided-goofballs)
      (coincided-goofball-help (set-diff log (widgets-coincide log))))
    
    
    ;; coincided-goofball-help : -> ListOfGoofball<%>
    ;; RETURNS: a List of Goofballs that have coincided without any duplicates
    ;; EXAMPLE: (coincided-goofball-help
    ;;            (list (make-stealth-goofball 10 10 2 2)
    ;;                  (make-stealth-goofball 10 10 3 4))) =>
    ;;           (list (make-stealth-goofball 10 10 2 2))
    ;; DESIGN STRATEGY: Cases based on lst
    (define (coincided-goofball-help lst)
      (cond
        [(empty? lst) empty]
        [(empty? (rest (rest lst))) empty]
        [else (cons (first lst) (coincided-goofball-help (rest (rest lst))))]))
    


    ;; process-goofballs : ListOfGoofball<%> (Goofball<%> -> Void) -> Void
    ;; GIVEN: a list of goofballs and a function
    ;; WHERE: the function updates each goofball in the list 
    ;; EFFECT: a list of goofballs updated to its state at the next event
    ;; STRATEGY: Combine Simpler Functions
    (define (process-goofballs fn)
      (for-each fn log))


    

    ;;-------------------------------------------------------------------
    ;;-------------------------------------------------------------------
    

    ;; TO-SCENE ->
    
    ;;; to-scene : -> Scene
    ;;; RETURNS: a Scene that depicts the state of the current SimulatorState
    ;;; STRATEGY: Use HOF Foldr on ss
    (define/public (to-scene)
      (explosions-to-scene
       (robots-to-scene
        (goofballs-to-scene
         EMPTY-CANVAS))))
    
    
    
    ;;; goofballs-to-scene : Scene -> Scene
    ;;; GIVEN: a scene
    ;;; RETURNS: a Scene that depicts the state of the list of goofballs
    ;;; STRATEGY: Use HOF Foldr on log
    (define (goofballs-to-scene scene)
      (foldr
       (lambda (goofball scene)
         (send goofball add-to-scene scene))
       scene
       log))
    
    
    
    ;;; robots-to-scene :  Scene -> Scene
    ;;; GIVEN: a scene
    ;;; RETURNS: a Scene that depicts the state of the list of robots
    ;;; STRATEGY: Use HOF Foldr on lor
    (define (robots-to-scene scene)
      (foldr
       (lambda (robot scene)
         (send robot add-to-scene scene))
       scene
       lor))
    
    
    ;;; explosions-to-scene : Scene -> Scene
    ;;; GIVEN: a scene
    ;;; RETURNS: a Scene that depicts the state of the list of explosions
    ;;; STRATEGY: Use HOF Foldr on loe
    (define (explosions-to-scene scene)
      (foldr
       (lambda (explosion scene)
         (send explosion add-to-scene scene))
       scene
       explosions))



    ;;-------------------------------------------------------------------
    ;;-------------------------------------------------------------------


    ;; AFTER-KEY-EVENT ->
    
    ;; after-key-event : KeyEvent -> Void
    ;; GIVEN: a key event 
    ;; EFFECT: updates this simulatorstate to its state following the given
    ;;         key event
    ;; STRATEGY: Cases based on kev
    (define/public (after-key-event kev)
      (cond
        [(key=? kev NEW-GOOFBALL) (world-after-n-key)]
        [(key=? kev NEW-DAGR) (world-after-r-key)]
        [(key=? kev NEW-SAGR) (world-after-s-key)]
        [(key=? kev Y-UPWARDS) (world-after-u-key)]
        [(key=? kev Y-DOWNWARDS) (world-after-d-key)]
        [(key=? kev SPACE) (begin (set! paused? (not paused?)) this)]
        [(velocity-change? kev)
         (process-goofballs (lambda (obj) (send obj after-key-event kev)))]
        [else this]))



    ;; world-after-n-key : -> Void
    ;; GIVEN: no arguments
    ;; EFFECT: adds a new goofball to the simulator-state
    (define (world-after-n-key)
      (begin
        (set! log
              (cons (make-stealth-goofball
                     INITIAL-X
                     INITIAL-Y
                     INITIAL-VX
                     INITIAL-VY) log))
        this))


    ;; world-after-r-key : -> Void
    ;; GIVEN: no arguments
    ;; EFFECT: adds a new DAGR robot to the simulator-state
    (define (world-after-r-key)
      (begin
        (set! lor
              (cons (make-DAGR (+ launch-x DAGR-OFFSET)
                               (- launch-y ROBOT-Y-OFFSET)
                               DAGR-OFFSET ROBOT-VY) lor))
        this))


    ;; world-after-s-key : -> Void
    ;; GIVEN: no arguments
    ;; EFFECT: adds a new SAGR robot to the simulator-state
    (define (world-after-s-key)
      (begin
        (set! lor
              (cons (make-SAGR (+ launch-x SAGR-OFFSET)
                               (- launch-y ROBOT-Y-OFFSET)
                               SAGR-OFFSET ROBOT-VY) lor))
        this))


    ;; velocity-change? : KeyEvent -> Boolean
    ;; GIVEN: a keyevent
    ;; RETURNS: true iff keyevent is right, left, up or down
    ;; EXAMPLE: (velocity-change? "right") => true
    ;; STRATEGY: Combine Simpler Functions
    (define (velocity-change? kev)
      (or (key=? kev RIGHT)
          (key=? kev LEFT)
          (key=? kev UP)
          (key=? kev DOWN)))
    


    ;; world-after-u-key : -> Void
    ;; EFFECT: may or may not change the launcher position in this
    ;;        simulatorstate
    ;; STRATEGY: Cases based on launch-y
    (define (world-after-u-key)
      (if (= launch-y ZERO)
          this
          (begin
            (set! launch-y (- launch-y LAUNCH-Y-LIMIT)) this)))


    ;; world-after-d-key : -> Void
    ;; EFFECT: may or may not change the launcher position in this
    ;;        simulatorstate
    ;; STRATEGY: Cases based on launch-y
    (define (world-after-d-key)
      (if (= launch-y CANVAS-HEIGHT)
          this
          (begin
            (set! launch-y (+ launch-y LAUNCH-Y-LIMIT)) this)))


    ;;-------------------------------------------------------------------
    ;;-------------------------------------------------------------------


    ;; AFTER-MOUSE-EVENT ->
    
    ;; after-mouse-event : Int Int MouseEvent -> Void
    ;; GIVEN: a location and what kind of mouse event
    ;; EFFECT: updates this simulatorstate to its state following the given 
    ;;     mouse event at the given location
    ;; STRATEGY: Cases on mev
    (define/public (after-mouse-event mx my mev)
      (cond
        [(mouse=? mev "button-down")
         (world-after-button-down mx my)]
        [(mouse=? mev "drag")
         (world-after-drag mx my)]
        [(mouse=? mev "button-up")
         (world-after-button-up mx my)]))

    
    ;; world-after-button-down : Int Int -> Void
    ;; GIVEN: a location 
    ;; EFFECT: updates simulatorstate to its state following button-down mouse
    ;;     event at the given location
    (define (world-after-button-down mx my)
      (process-goofballs
       (lambda (obj) (send obj after-button-down mx my))))
    

    ;; world-after-button-up : Int Int -> Void
    ;; GIVEN: a location 
    ;; EFFECT: updates simulatorstate to its state following button-up mouse
    ;;     event at the given location
    (define (world-after-button-up mx my)
      (process-goofballs
        (lambda (obj) (send obj after-button-up mx my))))



    ;; world-after-drag : Int Int -> Void
    ;; GIVEN: a location 
    ;; EFFECT: updates simulatorstate to its state following drag mouse
    ;;     event at the given location
    (define (world-after-drag mx my)
      (process-goofballs
        (lambda (obj) (send obj after-drag mx my))))




    ;;-------------------------------------------------------------------
    ;;-------------------------------------------------------------------
    
    ;; SAGR-BEHAVIOUR ->
    
    
    ;; robot-speed-change : ListOfRobot<%> -> ListOfRobot<%>
    ;; GIVEN: a list of robot
    ;; RETURNS: the given list of robots with changes in their speeds, if any.
    ;; EXAMPLE: See tests 
    ;; DESIGN STRATEGY: Combine Simpler Functions
    (define/public (robot-speed-change lor)
      (append (robot-modification (out-of-fuel (first (separate-sagrs lor))))
              (second (separate-sagrs lor))))
    
    
    ;; separate-sagrs : ListOfRobot<%> -> ListofX
    ;; GIVEN: list of robots
    ;; RETURNS: a list of X, where X could be SAGRs or DAGRs
    ;; EXAMPLE:
    ;; (separate-sagrs (list (make-SAGR 141 357 3 4) (make-DAGR 10 19 2 3)))
    ;;  => (list (list (make-SAGR 141 357 3 4)) (list (make-DAGR 10 19 2 3)))
    (define (separate-sagrs lor)
      (cond
        [(empty? lor) (list empty empty)]
        [else
         (local
           ((define (separation lor sagrs dagrs)
              (cond
                [(empty? lor) (list sagrs dagrs)]
                [else
                 (if (send (first lor) smart?)
                     (separation (rest lor) (cons (first lor) sagrs) dagrs)
                     (separation (rest lor) sagrs (cons (first lor) dagrs)))])))
           (separation lor empty empty))]))
    
    
    
    ;; out-of-fuel : ListOfRobot<%>(SAGR) -> ListOfRobot<%>(SAGR)
    ;; GIVEN: a list of SAGRs
    ;; RETURNS: a list of SAGRs that are inside the canvas and have not run out
    ;;          of fuel
    ;; EXAMPLE: (out-of-fuel (list (new SAGR [x 10][y 10][t 400][vx 6][vy 7]))
    ;;          => empty
    ;; STRATEGY: Use HOF filter on sagrs
    (define (out-of-fuel sagrs)
      (filter (lambda (x) (sagr-conditions x)) sagrs))
    
    
    
    ;; sagr-conditions : Robot<%> (SAGR) -> Boolean
    ;; GIVEN: a SAGR
    ;; RETURNS: true iff the SAGR is inside the canvas and has not run out
    ;;          of fuel
    ;; STRATEGY: Combine Simpler Functions
    (define (sagr-conditions sagr)
      (and (< (send sagr get-t) FUEL-LIMIT)
           (> (send sagr get-x) ZERO)
           (> (send sagr get-y) ZERO)
           (< (send sagr get-x) CANVAS-WIDTH)
           (< (send sagr get-y) CANVAS-HEIGHT)))
    
    
    
    ;; robot-modification : ListOfRobot<%>(SAGR) -> ListOfRobot<%>(SAGR)
    ;; GIVEN: a list of SAGRs
    ;; RETURNS: given list of SAGRs with their speed modified, if any
    ;; STRATEGY: change the speed of a SAGR if needed and return the entire list
    (define (robot-modification lor)
      (cond
        [(empty? lor) empty]
        [else (if (empty? (get-closest-goofball (first lor)))
                  (cons (first lor) (robot-speed-change (rest lor)))
                  (cons (send (first lor) change-speed
                              (get-closest-goofball (first lor)))
                        (robot-modification (rest lor))))]))
    
    
    
    ;; get-closest-goofball : Robot<%>(SAGR) -> Goofball<%>
    ;; GIVEN: a SAGR
    ;; RETURNS: the closest goofball to the given SAGR
    ;; STRATEGY: Combine Simpler Functions
    (define (get-closest-goofball r)
      (closest-ball r (get-detectable-balls r log)))
    
    
    ;; get-detectable-balls : Robot<%> ListOfGoofball<%> -> ListOfGoofball<%>
    ;; GIVEN: a SAGR and a list of goofballs
    ;; RETURNS: a list of goofballs that can be detected by the given robot
    ;; STRATEGY: Use HOF filter on log
    (define (get-detectable-balls r log)
      (filter
       (lambda (x) (or (not (send x stealthy?))
                       (<= (send r distance-measure x) 50))) log))
    
    
    
    
    ;; closest-ball: Robot<%> ListOfGoofball<%> -> Goofball<%>
    ;; GIVEN: a SAGR and a list of goofballs
    ;; RETURNS: a goofball that is closest to the robot
    ;; STRATEGY: store the closest-by-far goofball in g and return g when log
    ;;            becomes empty
    (define (closest-ball r log)
      (cond
        [(empty? log) empty]
        [else
         (local ((define (get-closest log g)
                   (cond
                     [(empty? log) g]
                     [else (if (<= (send r distance-measure (first log))
                                   (send r distance-measure g))
                               (get-closest (rest log) (first log))
                               (get-closest (rest log) g))])))
           (get-closest (rest log) (first log)))]))
    
    
    
    ;;-------------------------------------------------------------------
    ;;-------------------------------------------------------------------
    

    ;; OVERLAPS:
    
    ;; robots-overlap-goofballs : -> ListofX
    ;; GIVEN: a list of robots and a list of goofballs
    ;; RETURNS: a list of X where X could be list of robots or list of
    ;;          goofballs
    ;; STRATEGY: Check if the first element of robot overlaps with any of the
    ;;           goofballs; iterate over the list of robots
    (define/public (robots-overlap-goofballs)
      (local
        ((define (overlap lor new-lor new-log)
           (cond
             [(empty? lor) (list new-lor new-log)]
             [else (if (empty? (overlap-found (first lor) log))
                       (overlap (rest lor) new-lor new-log)
                       (overlap (rest lor) (cons (first lor) new-lor)
                                (append (overlap-found (first lor) log)
                                        new-log)))])))
        (overlap lor empty empty)))
    
    
    ;; overlap-found : Robot<%> ListOfGoofball<%> -> ListOfGoofball<%>
    ;; GIVEN: a robot and a list of goofball
    ;; RETURNS: a list of goofballs that overlap with the given robot
    ;; STRATEGY: iterate over the list of goofball and check every element 
    ;;   whether it overlaps with robot; if it does, add to list new-log
    (define (overlap-found r log)
      (local
        ((define (overlap log new-log)
           (cond
             [(empty? log) new-log]
             [else (if (empty? (goofball-type r (first log)))
                       (overlap (rest log) new-log)
                       (overlap (rest log)
                                (append
                                 (goofball-type r (first log)) new-log)))])))
        (overlap log empty)))


    ;; goofball-type : Robot<%> Goofball<%> -> ListOfGoofball<%>
    ;; GIVEN: a robot and a goofball
    ;; RETURNS: a list of that goofball if it overlaps with the given robot
    ;; STRATEGY: Cases based on stealthy?
    (define (goofball-type r g)
      (if (send g distance-measure r)
          (list g)
          empty))
    
    
    
    ;; stealth-to-round : -> ListOfGoofball<%>
    ;; RETURNS: a list of stealth goofballs that overlap with SAGR
    ;; STRATEGY: Combine Simpler Functions
    (define (stealth-to-round)
      (sagr-stealth-overlap (first (robots-overlap-goofballs))
                            (second (robots-overlap-goofballs))
                            empty))
    
    
    ;; sagr-stealth-overlap: ListOfRobot<%> ListOfGoofball<%> ListOfGoofball<%> 
    ;;                       -> ListOfGoofball<%>
    ;; GIVEN: a list of robot, a list of goofballs and a processed list of
    ;;       goofballs
    ;; RETURNS: a list of stealth goofballs that overlap with SAGR
    ;; STRATEGY: Check every element of lor whether it is a SAGR
    (define (sagr-stealth-overlap lor log lst)
      (cond
        [(empty? lor) lst]
        [else (if (send (first lor) smart?)
                  (sagr-stealth-overlap
                   (rest lor) log
                   (append (overlap-helper (first lor) log empty) lst))
                  (sagr-stealth-overlap (rest lor) log lst))]))
    
    
    
    ;; overlap-helper : Robot<%> ListOfGoofball<%> ListOfGoofball<%>
    ;;                 -> ListOfGoofball<%>
    ;; GIVEN: a robot and a list of goofball
    ;; RETURNS: a list of stealth goofballs that overlap with SAGR
    ;; STRATEGY: Check every element of log whether it is stealthy
    (define (overlap-helper r log lst)
      (cond
        [(empty? log) lst]
        [else (if (send (first log) stealthy?)
                  (overlap-helper r (rest log)
                                  (append (goofball-type r (first log)) lst))
                  (overlap-helper r (rest log) lst))]))



    ;;-------------------------------------------------------------------
    ;;-------------------------------------------------------------------
    

    ;; WIDGETS COLLISIONS
    
    
    ;; widgets-coincide : ListOfWidget -> ListOfWidget
    ;; GIVEN: A list of widgets
    ;; WHERE: widgets could be goofballs or robots
    ;; RETURNS: a given list of widgets without the ones collided
    ;; STRATEGY: Combine 2 functions where coincide checks the list for matches
    ;;           and is-rest-low-empty checks if the rest low is empty
    (define/public (widgets-coincide low)
      (local
        ((define (coincide low acc acc2)
           (cond
             [(empty? low) (append acc acc2 low)]
             [(empty? (rest low)) (append acc acc2 low)]
             [(get-coordinates low)
              (coincide (append (rest (rest low)) acc) '() acc2)]
             [else
              (local
                ((define (is-rest-low-empty low acc acc2)
                   (if (empty? (rest low))
                       (coincide acc (rest low) (cons (first low) acc2))
                       (coincide low acc acc2))))
                (is-rest-low-empty (cons (first low) (rest (rest low)))
                                   (cons (second low) acc)
                                   acc2))])))
        (coincide low empty empty)))
    
    
    ;; get-coordinates : ListOfWidget -> Boolean
    ;; GIVEN: A list of widgets
    ;; WHERE: widgets could be goofballs or robots
    ;; RETURNS: true if the first and second elements intersect
    (define/public (get-coordinates low)
      (send (first low) intersects? (second low)))
    
    
    
    
    ))



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;;; make-world : ListOfGoofball<%> ListOfRobot<%> -> SimulatorState<%>
;;; GIVEN: a list of Goofball<%> and a list of Robot<%>
;;; RETURNS: a world with those goofballs and robots
;;;     and the launcher at its standard initial position
(define (make-world log lor)
  (new SimulatorState% [launch-x 800][launch-y 500][log log][lor lor]
       [paused? true][explosions empty]))


;; TEST:
(check simulator-equal? (make-world empty empty)
       (new SimulatorState% [launch-y 500][log empty][lor empty][paused? true]
            [explosions empty]))



   
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;



;; Explosion Class

;; An Explosion is a (new Explosion% [x Integer] [y Integer] [counter Integer])
;; An Explosion represents site of destroyed goofballs


(define Explosion%
  (class* object% (StatefulWidget<%>)


    ;;----------------------------------------------------------------------

    
    (init-field x y counter) ; the x and y coordinate of location of explosion
                             ; counter, that keeps track of exploded goofballs


    ;; fields for images
    (field [SOLID "solid"])
    (field [RED "red"])
    (field [LENGTH 20])

    
    ;;----------------------------------------------------------------------
    
    
    (super-new)


    ;;----------------------------------------------------------------------

    
    ;; get-x : -> Integer
    ;; RETURNS: x coordinate of explosion
    (define/public (get-x)
      x)


    ;; get-y : -> Integer
    ;; RETURNS: y coordinate of explosion
    (define/public (get-y)
      y)

    
    ;; get-counter : -> Integer
    ;; RETURNS: time left for an explosion
    (define/public (get-counter)
      counter)

    ;; after-tick : -> Void
    ;; EFFECT: may or may not change the counter of the given explosion
    ;; STRATEFY: Cases based on counter
    (define/public (after-tick)
      (if (< counter ZERO)
          this
          (begin
            (set! counter (- counter ONE))
            this)))


    ;; after-to-scene : -> Scene
    ;; RETURNS: an Scene with this explosion painted on it
    ;; STRATEFY: Cases based on counter
    (define/public (add-to-scene scene)
      (if (< counter ZERO)
          this
          (place-image (square LENGTH SOLID RED)
                       x y scene)))

    
    ;; after-tick-while-paused : -> Void
    ;; EFFECT: may or may not change the counter of the given explosion
    ;; STRATEFY: Cases based on counter
    (define/public (after-tick-while-paused)
      (if (< counter ZERO)
          this
          (begin
            (set! counter (- counter ONE))
            this)))

    ;; the explosion doesn't have any other behaviors
   
    ;; after-button-down: -> Explosion<%>
    (define/public (after-button-down)
      this)

    ;; after-button-up: -> Explosion<%>
    (define/public (after-button-up)
      this)
    
    ;; after-drag: -> Explosion<%>
    (define/public (after-drag)
      this)
    

   ;; after-key-event: -> Explosion<%>
    (define/public (after-key-event)
      this)


    ))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;; GOOFBALL CLASS ->

;; A Goofball is a (new Goofball% [x PosInt] [y PosInt] [vx Int]
;;                               [vy Int] [is-stealthy? Boolean])
;; It represents a stealthy or a round goofball

(define Goofball%
  (class* object% (Goofball<%>)

    ;;---------------------------------------------------------------
    
    (init-field x y vx vy) ; the goofball's center x and y coordinates
    ; and velocity components vx and vy
    
    (init-field is-stealthy?) ; true if the goofball is stealthy
    
    (init-field [selected? false])
    ; Boolean, checks if the goofball is selected?
    
    (init-field [length 35])
    ; Default length of the stealthy goofball
    
    (init-field [radius 20])
    ; Default radius of the round goofball
    
    (init-field [saved-mx 0] [saved-my 0])
    ; Default values of mouse click coordinates
    
    
    
    (field [NORTH-WALL 0])
    ; represents value of y at upper wall of the canvas
    (field [SOUTH-WALL 600])
    ; represents value of y at lower wall of the canvas
    (field [RIGHT-WALL 800])
    ; represents value of x at east wall of the canvas
    (field [LEFT-WALL 0])
    ; represents value of x at west wall of the canvas
    (field [LENGTH 35])
    ; Default length of the stealthy goofball
    (field [HALF-LENGTH (/ LENGTH 2)])
    ; Half of length
    (field [RADIUS 20])
    ; Default radius of the round goofball
    
    
    
    
    ;; Constants for images
    (field [BLUE "blue"])
    (field [GRAY "gray"])
    (field [RED "red"])
    (field [GREEN "green"])
    (field [OUTLINE "outline"])
    (field [SOLID "solid"])
    (field [ORANGE "orange"])
    (field [TEXT-SIZE 10])
    (field [OPEN-BRACE "("])
    (field [CLOSE-BRACE ")"])
    (field [COMMA ","])
    
    
    ;;---------------------------------------------------------------
    
    (super-new)
    
    ;;---------------------------------------------------------------
    
    
    ;; -> PosInt
    ;; RETURNS: the x position of the center of this goofball
    (define/public (get-x)
      x)
    
    ;; -> PosInt
    ;; RETURNS: the y position of the center of this goofball
    (define/public (get-y)
      y)
    
    ;; -> PosInt
    ;; RETURNS: the vx component of this goofball's velocity
    (define/public (get-vx)
      vx)
    
    ;; -> PosInt
    ;; RETURNS: the vx component of this goofball's velocity
    (define/public (get-vy)
      vy)
    
    ;; -> Boolean
    ;; RETURNS: true iff this goofball has the stealth of a Stealth Goofball
    (define/public (stealthy?)
      is-stealthy?)
    

    ;;---------------------------------------------------------------
    ;;---------------------------------------------------------------
    

    ;; intersects? : Goofball<%> -> Boolean
    ;; GIVEN: a goofball
    ;; RETURNS: true iff the coordinates of this goofball and the given one
    ;;          are same
    ;; STRATEGY: Ask the other ball for its data
    (define/public (intersects? other-b)
      (coordinates-intersect?
       (send other-b get-x)
       (send other-b get-y)
       (send other-b get-vx)
       (send other-b get-vy)))


    ;; coordinates-intersect? : Int Int Int Int -> Boolean
    ;; GIVEN: the coordinates of goofball
    ;; RETURNS: true iff the coordinates of this goofball and the given one
    ;;          are same
    (define (coordinates-intersect? other-x other-y other-vx other-vy)
      (if (or (and (= (+ x vx) (+ other-x other-vx)) 
                   (= (+ y vy) (+ other-y other-vy)))
              (and (= x other-x) (= y other-y)))
          true
          false))



    ;; distance-measure : Robot<%> -> Boolean
    ;; GIVEN: a robot
    ;; RETURNS: true iff the robot intersects this goofball
    ;; STRATEGY: Ask the given Robot for its data
    (define/public (distance-measure r)
      (if is-stealthy?
          (distance-between-center (send r get-x)
                                   (send r get-y)
                                   (send r get-vx)
                                   (send r get-vy)
                                   (if (send r smart?) SAGR-LENGTH DAGR-LENGTH)
                                   (* HALF length))
          (distance-between-center (send r get-x)
                                   (send r get-y)
                                   (send r get-vx)
                                   (send r get-vy)
                                   (if (send r smart?) SAGR-LENGTH DAGR-LENGTH)
                                   radius)))


    ;; distance-between-center : Integer^6 -> Boolean
    ;; GIVEN: parameters of robot
    ;; RETURNS: true iff robot and this goofball overlap 
    (define (distance-between-center rx ry rvx rvy rl arg)
      (or (<= (sqrt (+ (sqr (- rx x))
                       (sqr (- ry y))))
              (+ (* HALF rl) arg))   
          (<= (sqrt (+ (sqr (- (+ rx rvx)
                               (+ x vx)))
                       (sqr (- (+ ry rvy)
                               (+ y vy)))))
              (+ (* HALF rl) arg))))


    ;; change-speed : Goofball<%> -> Void
    ;; GIVEN: a goofball
    ;; EFFECT: has no effect on the this goofball
    (define/public (change-speed g)
      this)
     


    ;;---------------------------------------------------------------
    ;;---------------------------------------------------------------
    

    ;; AFTER-TICK ->

    ;; after-tick : -> Void
    ;; GIVEN: no arguments
    ;; EFFECT: may or may not change the parameters of this goofball
    ;; STRATEGY: Cases based on selected?
    (define/public (after-tick)
      (if selected?
          this
          (goofball-one-second-later)))


      
    ;; after-tick-while-paused : -> Void
    ;; EFFECT: does not change the parameters of this goofball
    (define/public (after-tick-while-paused)
      this)


    
    ;;---------------------------------------------------------------
    ;;---------------------------------------------------------------

    ;; AFTER-KEY-EVENT ->
    

    ;; after-key-event : KeyEvent -> Void
    ;; EFFECT: updates this goofball to its state following the
    ;;     given key event
    ;; STRATEGY: Cases based on selected?
    (define/public (after-key-event ke)
      (if selected?
          (cond
            [(key=? ke RIGHT) (begin (set! vx (+ vx ONE)) this)]
            [(key=? ke LEFT) (begin (set! vx (- vx ONE)) this)]
            [(key=? ke UP) (begin (set! vy (- vy ONE)) this)]
            [(key=? ke DOWN) (begin (set! vy (+ vy ONE)) this)])
          this))



    ;;---------------------------------------------------------------
    ;;---------------------------------------------------------------
    

    ;; AFTER-MOUSE-EVENTS ->


    ;; after-button-down : Int Int -> Void
    ;; GIVEN: location of mouse event
    ;; EFFECT: updates this goofball to the state it should have
    ;; following the specified mouse event at the given location.
    (define/public (after-button-down mx my)
      (if (in-goofball? mx my)
          (begin
            (set! selected? true)
            (set! saved-mx (- mx x))
            (set! saved-my (- my y)))
          this))
    
    
    ;; in-goofball? : PosInt PosInt -> Boolean
    ;; GIVEN: A location (x and y coordinates)
    ;; RETURNS: true iff the location lies within the goofball
    ;; STRATEGY: Cases based on is-stealthy?
    (define (in-goofball? mx my)
      (if is-stealthy?
          (and
           (<= (- x (goofball-at x y HALF-LENGTH))
               mx
               (+ x (goofball-at x y HALF-LENGTH)))
           (<= (- y (goofball-at x y HALF-LENGTH))
               my
               (+ y (goofball-at x y HALF-LENGTH))))
          (<= (+ (sqr (- x mx)) (sqr (- y my)))
              (sqr (goofball-at x y RADIUS)))))
    
    

    ;; after-drag : Int Int -> Void
    ;; GIVEN: location of mouse event
    ;; EFFECT: updates this goofball to the state it should have
    ;; following the specified mouse event at the given location.
    ;; STRATEGY: Cases based on selected?
    (define/public (after-drag mx my)
      (if selected?
          (begin
            (set! x (- mx saved-mx))
            (set! y (- my saved-my)))
          this))
    

    ;; after-button-up : Int Int -> Void
    ;; GIVEN: location of mouse event
    ;; EFFECT: updates this goofball to the state it should have
    ;; following the specified mouse event at the given location.
    (define/public (after-button-up mx my)
      (if (in-goofball? mx my)
          (begin
            (set! selected? false))
          this))
    
    
    
    
    
    ;;---------------------------------------------------------------
    ;;---------------------------------------------------------------
    

    ;; ADD-TO-SCENE ->
    
    
    ;; add-to-scene: Scene -> Scene
    ;; GIVEN: a scene
    ;; RETURNS: a scene like the given one, but with this Goofball
    ;;         painted on it.
    ;; STRATEGY: Cases based on is-stealthy?
    (define/public (add-to-scene scene)
      (if is-stealthy?
          (check-stealthy scene)
          (check-round scene)))
    
    
    ;; check-stealthy: Scene -> Scene
    ;; GIVEN: a scene
    ;; RETURNS: a scene like the given one, but with this Stealthy Goofball
    ;;         painted on it.
    ;; STRATEGY: Cases based on attributes of this Stealthy Goofball
    (define (check-stealthy scene)
      (cond
        [(< length LENGTH) (red-goofball square length scene)]
        [selected? (green-goofball square length scene)]
        [else (normal-goofball square length GRAY scene)]))
    
    
    ;; check-round : Scene -> Scene
    ;; GIVEN: a scene
    ;; RETURNS: a scene like the given one, but with this Round Goofball
    ;;         painted on it.
    ;; STRATEGY: Cases based on attributes of this Round Goofball
    (define (check-round scene)
      (cond
        [(< radius RADIUS) (red-goofball circle radius scene)]
        [selected? (green-goofball circle radius scene)]
        [else (normal-goofball circle radius BLUE scene)]))
    
    
    ;; red-goofball : Symbol PosInt Scene -> Scene
    ;; GIVEN: a shape (circle/square), an argument (length/radius) and a scene
    ;; RETURNS:  a scene like the given one, but with this red Goofball
    ;;         painted on it.
    ;; STRATEGY: Combine Simpler Functions
    (define (red-goofball shape arg scene)
      (place-image (shape arg OUTLINE RED)
                   x y scene))
    
    
    ;; green-goofball : Symbol PosInt Scene -> Scene
    ;; GIVEN: a shape (circle/square), an argument (length/radius) and a scene
    ;; RETURNS:  a scene like the given one, but with this green Goofball
    ;;         painted on it.
    ;; STRATEGY: Combine Simpler Functions
    (define (green-goofball shape arg scene)
      (place-image (circle 4 SOLID ORANGE)
                   (+ x saved-mx) (+ y saved-my)
                   (place-image (shape arg OUTLINE GREEN)
                                x y scene)))
    
    
    ;; normal-goofball : Symbol PosInt String Scene -> Scene
    ;; GIVEN: a shape (circle/square), an argument (length/radius), a color
    ;;        and a scene
    ;; RETURNS:  a scene like the given one, but with this normal Goofball
    ;;         painted on it.
    ;; WHERE: a normal goofball is the one with default length/radius
    ;; STRATEGY: Combine Simpler Functions
    (define (normal-goofball shape arg color scene)
      (place-image (display-velocity shape arg color)
                   x y scene))
    
    
    
    ;; display-velocity : Symbol PosInt String -> Image
    ;; GIVEN: a shape (circle/square), an argument (length/radius), a color
    ;; RETURNS: an Image with velocity components of goofball displayed
    ;; STRATEGY: Combine Simpler Functions
    (define (display-velocity shape arg color)
      (overlay
       (text (string-append OPEN-BRACE (number->string vx)
                            COMMA (number->string vy) CLOSE-BRACE)
             TEXT-SIZE color)
       (shape arg OUTLINE color)))



    ;;---------------------------------------------------------------
    ;;---------------------------------------------------------------


    ;; GOOFBALL-ONE-SECOND-LATER ->
    

    ;; goofball-one-second-later : -> Void
    ;; EFFECT: updates the position of this goofball to its state at the next
    ;;  tick
    ;; STRATEGY: Cases based on is-stealthy?
    (define/public (goofball-one-second-later)
      (if is-stealthy?
          (either-goofball HALF-LENGTH)
          (either-goofball RADIUS)))
    
    
    ;; either-goofball : -> Void
    ;; EFFECT: updates the position of this goofball after one tick
    ;; STRATEGY: Cases based on center coordinates 
    (define (either-goofball arg)
      (if (or (< (+ x vx) LEFT-WALL) (> (+ x vx) RIGHT-WALL)
              (< (+ y vy) NORTH-WALL) (> (+ y vy) SOUTH-WALL))
          (perfect-bounce arg)
          (begin
            (set! x (+ x vx))
            (set! y (+ y vy))
            (length-or-radius)
            this)))
    

   
    ;; perfect-bounce : Int -> Void
    ;; EFFECT: updates the position of this goofball after perfect-bounce
    ;; STRATEGY: Cases based on attributes of goofball
    (define (perfect-bounce arg)
      (cond
        [(perfect-edge-condition? arg)
         (begin
           (set! x (constraint-x (+ x vx)))
           (set! y (constraint-y (+ y vy)))
           (set! vx (- ZERO vx))
           (set! vy (- ZERO vy))
           (length-or-radius)
           this)]
        [(condition-1 arg)
         (begin
           (set! x (constraint-x (+ x vx)))
           (set! y (constraint-y (+ y vy)))
           (set! vx (- ZERO vx))
           (length-or-radius)
           this)]
        [(condition-2 arg)
         (begin
           (set! x (constraint-x (+ x vx)))
           (set! y (constraint-y (+ y vy)))
           (set! vy (- ZERO vy))
           (length-or-radius)
           this)]))

    
    ;; length-or-radius : -> Void
    ;; GIVEN: no arguments
    ;; EFFECT: updates length/radius of this goofball based on is-stealthy?
    ;; STRATEGY: Cases based on is-stealthy?
    (define (length-or-radius)
      (if is-stealthy?
          (set! length (* TWICE (goofball-at x y HALF-LENGTH)))
          (set! radius (goofball-at x y RADIUS))))
    
    
    
    
    ;; perfect-edge-condition? : PosInt -> Boolean
    ;; GIVEN: a length/radius
    ;; RETURNS: true if below conditions satisfy
    ;; STRATEGY: Cases based on attributes of goofball
    (define (perfect-edge-condition? arg)
      (or
       (and (<= (+ x vx) arg) (<= (+ y vy) arg) (= x y))
       (and (<= (+ x vx) arg) (>= (+ y vy) (- SOUTH-WALL arg))
            (= x (- SOUTH-WALL y)))
       (and (>= (+ x vx) (- RIGHT-WALL arg)) (<= (+ y vy) arg)
            (= (- RIGHT-WALL x) y))
       (and (>= (+ x vx) (- RIGHT-WALL arg)) (>= (+ y vy) (- SOUTH-WALL arg))
            (= (- RIGHT-WALL x) (- SOUTH-WALL y)))))
    
    
    
    
    ;; condition-1 : PosInt -> Boolean
    ;; GIVEN: a length/radius
    ;; RETURNS: true if below conditions satisfy
    ;; STRATEGY: Cases based on attributes of goofball
    (define (condition-1 arg)
      (or (and (<= (constraint-x (+ x vx)) arg)
               (< arg (constraint-y (+ y vy)) (- SOUTH-WALL arg)))
          (and (>= (constraint-x (+ x vx)) (- RIGHT-WALL arg))
               (< arg (constraint-y (+ y vy)) (- SOUTH-WALL arg)))))
    
    
    ;; condition-2 : PosInt -> Boolean
    ;; GIVEN: a length/radius
    ;; RETURNS: true if below conditions satisfy
    ;; STRATEGY: Cases based on attributes of goofball
    (define (condition-2 arg)
      (or (and (<= (constraint-y (+ y vy)) arg)
               (< arg (constraint-x (+ x vx)) (- RIGHT-WALL arg)))
          (and (>= (constraint-y (+ y vy)) (- SOUTH-WALL arg))
               (< arg (constraint-x (+ x vx)) (- RIGHT-WALL arg)))))
    
    
    
    
    
    ;; goofball-at : PosInt PosInt PosInt -> PosInt
    ;; GIVEN: center coordinates and wall offset value of goofball
    ;; RETURNS: length/radius of the goofball
    ;; EXAMPLE: (goofball-at 78 98 RADIUS) => 20
    ;; STRATEGY: Cases based on attributes of goofball
    (define/public (goofball-at x y arg)
      (cond
        [(and (< x arg) (< arg y (- SOUTH-WALL arg))) x]
        [(and (> x (- RIGHT-WALL arg))
              (< arg y (- SOUTH-WALL arg))) (- RIGHT-WALL x)]
        [(and (< y arg) (< arg x (- RIGHT-WALL arg))) y]
        [(and (> y (- SOUTH-WALL arg))
              (< arg x (- RIGHT-WALL arg))) (- SOUTH-WALL y)]
        [else (goofball-at-extend x y arg)]))
    
    
    ;; goofball-at-extend : PosInt PosInt PosInt -> PosInt
    ;; GIVEN: center coordinates and wall offset value of goofball
    ;; RETURNS: length/radius of the goofball
    ;; STRATEGY: Cases based on attributes of goofball
    (define (goofball-at-extend x y arg)
      (cond
        [(and (< x arg) (< y arg) (< x y)) x]
        [(and (< x arg) (< y arg) (> x y)) y]
        [(and (< x arg) (> y (- SOUTH-WALL arg)) (< x (- SOUTH-WALL y))) x]
        [(and (< x arg) (> y (- SOUTH-WALL arg)) (> x (- SOUTH-WALL y)))
         (- SOUTH-WALL y)]
        [(and (> x (- RIGHT-WALL arg)) (< y arg) (< (- RIGHT-WALL x) y))
         (- RIGHT-WALL x)]
        [(and (> x (- RIGHT-WALL arg)) (< y arg) (> (- RIGHT-WALL x) y)) y]
        [(and (> x (- RIGHT-WALL arg)) (> y (- SOUTH-WALL arg))
              (< (- RIGHT-WALL x) (- SOUTH-WALL y))) (- RIGHT-WALL x)]
          [(and (> x (- RIGHT-WALL arg)) (> y (- SOUTH-WALL arg))
                (> (- RIGHT-WALL x) (- SOUTH-WALL y))) (- SOUTH-WALL y)]
          [else arg]))
      




      ;; constraint-x : Integer -> Integer
      ;; GIVEN: A goofball's x-coordinate that might be outside the arena
      ;; RETURNS: what that coordinate would be after perfect bounces
      ;; EXAMPLES: (constrained-x (+ -150 -560)) => 90
      ;;           (constrained-x (+ 180 80)) => 140
      ;; DESIGN STRATEGY: Recur on x; halt when LEFT-WALL <= x <= RIGHT-WALL.
      ;; Halting measure : absolute value of x
      ;; Termination argument:  The absolute value of x decreases with each
      ;; recursive call, since we either call this on 2(RIGHT-WALL-X) - x or
      ;; 2(LEFT-WALL) - x.  The condition x > RIGHT-WALL  implies
      ;; 2(RIGHT-WALL) < 2x and so 2(RIGHT-WALL) - x < x.  Similarly
      ;; x < LEFT-WALL implies 2(LEFT-WALL) > 2x and 2(LEFT-WALL)-x > x,
      ;; which means the absolute value is decreasing because x is negative in
      ;; that case. Since the absolute value strictly decreases, and the 
      ;; function terminates when the absolute value is 200 or less, this must
      ;; terminate.
      (define (constraint-x x)
        (cond
          [(< x LEFT-WALL) (constraint-x (+ LEFT-WALL (- LEFT-WALL x)))]
          [(> x RIGHT-WALL) (constraint-x (- RIGHT-WALL (- x RIGHT-WALL)))]
          [else x]))
      
      
      ;; constraint-y : Integer -> Integer
      ;; GIVEN: A goofball's y-coordinate that might be outside the arena
      ;; RETURNS: what that coordinate would be after perfect bounces
      ;; EXAMPLES: (constrained-y (+ -150 -560)) => 90
      ;;           (constrained-y (+ 180 80)) => 140
      ;; DESIGN STRATEGY: Recur on y; halt when NORTH-WALL <= y <= SOUTH-WALL.
      ;; Halting measure : absolute value of y
      ;; Termination argument:  The absolute value of y decreases with each
      ;; recursive call, since we either call this on 2(SOUTH-WALL) - y or
      ;; 2(NORTH-WALL) - y.  The condition y > SOUTH-WALL  implies
      ;; 2(SOUTH-WALL) < 2y and so 2(SOUTH-WALL) - y < y.  Similarly
      ;; y < NORTH-WALL implies 2(NORTH-WALL) > 2y and 2(NORTH-WALL)-y > y,
      ;; which means the absolute value is decreasing because y is negative in
      ;; that case. Since the absolute value strictly decreases, and the 
      ;; function terminates when the absolute value is 200 or less, this must
      ;; terminate.
      (define (constraint-y y)
        (cond
          [(< y NORTH-WALL) (constraint-y (+ NORTH-WALL (- NORTH-WALL y)))]
          [(> y SOUTH-WALL) (constraint-y (- SOUTH-WALL (- y SOUTH-WALL)))]
          [else y]))
      
      
    
    
    ))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;



;;; make-stealth-goofball : Integer Integer Integer Integer -> Goofball<%>
;;; GIVEN: x and y coordinates and velocity components vx and vy
;;; RETURNS: A Stealth Goofball with its center at the x and y coordinates
;;;     and velocity components vx and vy
;;; EXAMPLE:
;;; (make-stealth-goofball 33 34 -3 -3) =>
;;; (new Goofball% [x 33] [y 34] [vx -3] [vy -3] [is-stealthy? true])
(define (make-stealth-goofball x y vx vy)
  (new Goofball% [x x] [y y] [vx vx] [vy vy] [is-stealthy? true]))


;;; make-round-goofball : Integer Integer Integer Integer -> Goofball<%>
;;; GIVEN: x and y coordinates and velocity components vx and vy
;;; RETURNS: A Round Goofball with its center at the x and y coordinates
;;;     and velocity components vx and vy
;;; EXAMPLE:
;;; (make-round-goofball 33 34 -3 -3) =>
;;; (new Goofball% [x 33] [y 34] [vx -3] [vy -3] [is-stealthy? false])
(define (make-round-goofball x y vx vy)
  (new Goofball% [x x] [y y] [vx vx] [vy vy] [is-stealthy? false]))



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;



;; DAGR CLASS ->

;; A DAGR is a (make-DAGR Int Int Int Int)

(define DAGR%
  (class* object% (Robot<%>)
    
    ;;-----------------------------------------------------------------

    ;; the init-fields are the values that may vary from one Dumb
    ;; Anti-Goofball Robot (DAGR)  to the next.
    
    ; the x and y position of the center of the DAGR
    ; the DAGR's speed in x direction, in pixels/tick
    ; the DAGR's speed in y direction, in pixels/tick
    (init-field x y vx vy)
    (init-field [is-smart? false])
    
    
    ;; private data for objects of this class.
    ;; these can depend on the init-fields.
    
    ; the DAGR's length
    (field [length DAGR-LENGTH])
   
    
    ; image for displaying the DAGR
    (define SOLID "solid")
    (define BLACK "black")
    (field [DAGR-IMG (square length SOLID BLACK)])
    
    ;;-----------------------------------------------------------------

    
    (super-new)


    ;;-----------------------------------------------------------------
    
    ; return the x coordinate of the center of DAGR
    (define/public (get-x)
      x)
    ; return the y coordinate of the center of DAGR
    (define/public (get-y)
      y)
    ; return the speed in x direction of the DAGR
    (define/public (get-vx)
      vx)
    ; return the speed in y direction of the DAGR
    (define/public (get-vy)
      vy)
    ; is the DAGR smart? Default is false.
    (define/public (smart?)
      false)


    ;;---------------------------------------------------------------
    ;;---------------------------------------------------------------
    
    
    ;; after-tick : -> Void
    ;; EFFECT: updates this robot to its state following a tick
    ;;     while the world is not paused
    (define/public (after-tick)
      (begin
        (set! x (+ x vx))
        (set! y (+ y vy))
        this))
    
    ;; after-key-event : KeyEvent -> Void
    ;; GIVEN: key event
    ;; DETAILS: a DAGR ignores key events
    (define/public (after-key-event kev)
      this)      
    
    ;; after-button-down : Integer Integer -> Void
    ;; GIVEN: the location of a button-down event
    ;; DETAILS: a DAGR ignores all mouse events
    (define/public (after-button-down mx my)
      this)
    
    
    ;; after-button-up : Integer Integer -> Void
    ;; GIVEN: the location of a button-up event
    ;; DETAILS: a DAGR ignores all mouse events
    (define/public (after-button-up mx my)
      this)
    
    
    ;; after-drag : Integer Integer -> Void
    ;; GIVEN: the location of a drag event
    ;; DETAILS: a DAGR ignores all mouse events
    (define/public (after-drag mx my)
      this)
    
    
    
    ;; to-scene : Scene -> Scene
    ;; RETURNS: a scene like the given one, but with this DAGR painted
    ;; on it.
    (define/public (add-to-scene scene)
      (place-image DAGR-IMG x y scene))
    


    ;; after-tick-while-paused : -> Void
    ;; GIVEN: no arguments
    ;; EFFECT: updates this robot to its state following a tick
    ;;     in which the world is paused
    (define/public (after-tick-while-paused)
      this)
    
    
    ;;---------------------------------------------------------------
    ;;---------------------------------------------------------------
    
    
    
    ;; intersects? : Robot<%> -> Boolean
    ;; GIVEN: a robot
    ;; RETURNS: true iff the given robot intersects with this robot
    ;; STRATEGY: Ask the other robot for its data
    (define/public (intersects? other-r)
      (robs-overlaps? (send other-r get-x)
                      (send other-r get-y)
                      (send other-r get-vx)
                      (send other-r get-vy)
                      (if (send other-r smart?) SAGR-LENGTH DAGR-LENGTH)))
    
    
    ;; robs-overlaps? : Integer^5 -> Boolean
    ;; GIVEN: data of the other robot
    ;; RETURNS: true iff the other robot intersects with this robot
    ;; STRATEGY: Combine Simpler Functions
    (define (robs-overlaps? rx ry rvx rvy rl)
      (or (<= (sqrt (+ (sqr (- rx x)) (sqr (- ry y))))
              (+ (* HALF rl) (* HALF DAGR-LENGTH)))   
          (<= (sqrt (+ (sqr (- (+ rx rvx) (+ x vx)))
                       (sqr (- (+ ry rvy) (+ y vy)))))
              (+ (* HALF rl) (* HALF DAGR-LENGTH)))))
    
    
    
    ;; distance-measure : Robot<%> -> Void
    ;; GIVEN: a robot
    ;; RETURNS: has no effect on this robot
    (define/public (distance-measure r)
      this)
    
    
    ;; change-speed : Goofball<%> -> Void
    ;; GIVEN: a goofball
    ;; RETURNS: has no effect on this robot
    (define/public (change-speed g)
      this)
    
    
    
     
    ))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;



;; SAGR CLASS ->

;; A SAGR is a (make-SAGR PosInt PosInt Int Int)


(define SAGR%
  (class* object% (Robot<%>)

    ;;-----------------------------------------------------------------
    
    ;; the init-fields are the values that may vary from one Smart
    ;; Anti-Goofball Robot (SAGR)  to the next.
    
    ; the x and y position of the center of the SAGR
    ; and the "life" of the SAGR in the unit of ticks
    ; the SAGR's speed in x direction, in pixels/tick
    ; the SAGR's speed in y direction, in pixels/tick
    (init-field x y t vx vy)
    (init-field [is-smart? true])
    
    
    ;; private data for objects of this class.
    ;; these can depend on the init-fields.
    
    ; the SAGR's radius
    (field [length SAGR-LENGTH])
    (field [LESSER-VELOCITY -8])
    (field [GREATER-VELOCITY 8])
    
    
    ; image for displaying the SAGR
    (define SOLID "solid")
    (define ORANGE "orange")
    (field [SAGR-IMG (square length SOLID ORANGE)])
    
    ;;-----------------------------------------------------------------

    
    (super-new)


    ;;-----------------------------------------------------------------
    
    ; return the x coordinate of the center of SAGR
    (define/public (get-x)
      x)
    ; return the y coordinate of the center of SAGR
    (define/public (get-y)
      y)
    ; return the "life" t of the SAGR
    (define/public (get-t)
      t)
    ; return the speed in x direction of the SAGR
    (define/public (get-vx)
      vx)
    ; return the speed in y direction of the SAGR
    (define/public (get-vy)
      vy)
    ; is the SAGR smart? Default is false.
    (define/public (smart?)
      true)
    
    
    ;; after-tick : Time -> Void
    ;; GIVEN: no arguments
    ;; EFFECT: updates this robot to its state following a tick
    ;;     while the world is not paused
    (define/public (after-tick)
      (begin
        (set! x (+ x vx))
        (set! y (+ y vy))
        (set! t (+ t INCREMENT))
        this))


    
    ;; after-key-event : KeyEvent -> Void
    ;; GIVEN: a key event
    ;; DETAILS: a SAGR ignores key events
    (define/public (after-key-event kev)
      this)
    
    
    ;; after-button-down : Integer Integer -> Void
    ;; GIVEN: the location of a button-down event
    ;; DETAILS: a SAGR ignores all mouse events
    (define/public (after-button-down mx my)
      this)
    
    
    ;; after-button-up : Integer Integer -> Void
    ;; GIVEN: the location of a button-up event
    ;; DETAILS: a SAGR ignores all mouse events
    (define/public (after-button-up mx my)
      this)
    
    
    ;; after-drag : Integer Integer -> Void
    ;; GIVEN: the location of a drag event
    ;; DETAILS: a SAGR ignores all mouse events
    (define/public (after-drag mx my)
      this)
    
    
    
    ;; add-to-scene : Scene -> Scene
    ;; RETURNS: a scene like the given one, but with this SAGR painted
    ;; on it.
    (define/public (add-to-scene scene)
      (place-image SAGR-IMG x y scene))
    
    
    ;; after-tick-while-paused: -> Void
    ;; GIVEN: no arguments
    ;; EFFECT: updates this robot to its state following a tick
    ;;     in which the world is paused
    (define/public (after-tick-while-paused)
      this)



    ;; intersects? : Robot<%> -> Boolean
    ;; GIVEN: a robot 
    ;; RETURNS: true iff the given robot intersects with this robot
    ;; STRATEGY: Ask the other robot for its data
    (define/public (intersects? other-r)
      (robs-overlaps? (send other-r get-x)
                      (send other-r get-y)
                      (send other-r get-vx)
                      (send other-r get-vy)
                      (if (send other-r smart?) SAGR-LENGTH DAGR-LENGTH)))
    


    ;; robs-overlaps? : Integer^5 -> Boolean
    ;; GIVEN: data of the other robot
    ;; RETURNS: true iff the given robot intersects with this robot
    ;; STRATEGY: Combine SImpler Functions
    (define (robs-overlaps? rx ry rvx rvy rl)
      (or (<= (sqrt (+ (sqr (- rx x))
                       (sqr (- ry y))))
              (+ (* HALF rl) (* HALF SAGR-LENGTH)))   
          (<= (sqrt (+ (sqr (- (+ rx rvx)
                               (+ x vx)))
                       (sqr (- (+ ry rvy)
                               (+ y vy)))))
              (+ (* HALF rl) (* HALF SAGR-LENGTH)))))



    ;; distance-measure : Goofball<%> -> Integer
    ;; GIVEN: a goofball
    ;; RETURNS: the distance between given goofball and this robot
    ;; STRATEGY: Ask the goofball for its data
    (define/public (distance-measure goofball)
      (check-distance (send goofball get-x)
                      (send goofball get-y)
                      (send goofball get-vx)
                      (send goofball get-vy)))
    

    ;; check-distance : Integer^4 -> Integer
    ;; GIVEN: data of the other goofball
    ;; RETURNS: the distance between the centers of the goofball and this robot
    ;; STRATEGY: Combine Simpler Functions
    (define (check-distance gx gy gvx gvy)
      (sqrt (+ (sqr (- (+ gx gvx) (+ x vx)))
               (sqr (- (+ gy gvy) (+ y vy))))))
    


    ;; change-speed: Goofball<%> -> Void
    ;; GIVEN: a goofball
    ;; EFFECT: speed change in this robot in accordance to the given goofball
    ;; STRATEGY: Ask the goofball for its data
    (define/public (change-speed g)
      (modify-robot (send g get-x)
                    (send g get-y)))

    
    ;; modify-robot : Integer^2 -> Void
    ;; GIVEN: coordinates of goofball
    ;; EFFECT: speed change in this robot in accordance to the given goofball
    ;; STRATEGY: Cases based on difference in coordinates of goofball and this
    ;;           robot
    (define (modify-robot gx gy)
      (cond
        [(< gx x) (change-vy LESSER-VELOCITY gy)]
        [(= gx x) (change-vy ZERO gy)]
        [(> gx x) (change-vy GREATER-VELOCITY gy)]))


    ;; change-vy : Integer Integer -> Void
    ;; GIVEN: vx component of this robot and y-coordinate of goofball
    ;; EFFECT: speed change in this robot in accordance to the given goofball
    ;; STRATEGY: Cases based on difference in coordinates of goofball and this
    ;;           robot
    (define (change-vy vx-comp gy)
      (cond
        [(< gy y) (begin (set! vx vx-comp)
                         (set! vy LESSER-VELOCITY) this)]
        [(= gy y) (begin (set! vx vx-comp)
                         (set! vy ZERO) this)]
        [(> gy y) (begin (set! vx vx-comp)
                         (set! vy GREATER-VELOCITY) this)]))

    ))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;;; make-DAGR : Integer Integer Integer Integer -> Robot<%>
;;; GIVEN: x and y coordinates and velocity components vx and vy
;;; RETURNS: A DAGR with its center at the x and y coordinates
;;;     and velocity components vx and vy
;;; EXAMPLE:
;;; (make-DAGR 800 500 3 5) => (new DAGR% [x 800] [y 500] [vx 3] [vy 5])
(define (make-DAGR X Y VX VY)
  (new DAGR% [x X][y Y][vx VX][vy VY]))



;;; make-SAGR : Integer Integer Integer Integer -> Robot<%>
;;; GIVEN: x and y coordinates and velocity components vx and vy
;;; RETURNS: A DAGR with its center at the x and y coordinates
;;;     and velocity components vx and vy
;;; EXAMPLE:
;;; (make-SAGR 800 500 3 5) => (new SAGR% [x 800] [y 500] [t 0] [vx 3] [vy 5])
(define (make-SAGR X Y VX VY)
  (new SAGR% [x X][y Y][t 0][vx VX][vy VY]))




;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;



;; TESTS:

(begin-for-test

  (local
    ((define r1 (make-round-goofball 50 50 1 1))
     (define r2 (make-round-goofball 150 150 1 1))
     (define r3 (make-round-goofball 250 250 1 1))
     (define r4 (make-round-goofball 350 350 1 1))
     (define s1 (make-SAGR 390 390 1 1))
     (define ss1 (make-world (list r4 r1 r2 r3) (list s1)))
     (define ss2 (make-world (list r1 r2 r3 r4) (list s1))))
    (check-equal? (send (first (send ss1 robot-speed-change (list s1))) get-x)
                  390)
    (check-equal? (send ss1 launcher-x) 800))

  (local
    ((define d1 (make-DAGR 340 450 5 3))
     (define sg1 (make-stealth-goofball 545 130 2 1)))
    (check robot-equal? (send d1 distance-measure sg1) d1)
    (check robot-equal? (send d1 change-speed sg1) d1)
    (check goofball-equal? (send sg1 change-speed d1) sg1)
    (check-equal? (send sg1 distance-measure d1) false))

  (local
    ((define ss1 (make-world empty empty)))
    (check simulator-equal? (world-after-key-event ss1 "g") ss1))

  (local
    ((define s1 (make-SAGR 348 458 5 3))
     (define s2 (make-SAGR 500 100 2 1))
     (define sg1 (make-stealth-goofball 545 130 2 1))
     (define sg2 (make-round-goofball 100 500 2 1))
     (define ss1 (make-world empty (list s1 s2)))
     (define ss2 (make-world (list sg2 sg1) (list s1 s2))))
    (check-equal? (list-equal? (send ss1 robot-speed-change (list s1 s2))
                  (list s2 s1) 2) true)
    (check-equal? (send (first (send ss2 robot-speed-change (list s1 s2)))
                        smart?) true))

  (local
    ((define d1 (make-DAGR 340 450 5 3))
     (define s1 (make-SAGR 348 458 5 3))
     (define d2 (make-DAGR 348 458 5 3))
     (define s2 (make-SAGR 367 474 5 3))
     (define d3 (make-DAGR 500 100 2 1))
     (define d4 (make-DAGR 502 101 2 1))
     (define ss1 (make-world empty (list d1 s1 d3)))
     (define ss3 (make-world empty (list d3)))
     (define ss2 (new SimulatorState% [launch-y 500][log empty]
                     [lor (list d1 s1 d3)][paused? false][explosions empty]))
     (define ss4 (new SimulatorState% [launch-y 500][log empty]
                     [lor (list d4)][paused? false][explosions empty])))
    (check-equal? (send d1 intersects? s1) true)
    (check-equal? (send d1 intersects? d2) true)
    (check-equal? (send s1 intersects? d1) true)
    (check-equal? (send s1 intersects? s2) false)
    (check simulator-equal? (world-after-tick ss1) ss3)
    (check simulator-equal? (world-after-tick ss2) ss4))

  (local
    ((define d1 (make-DAGR 340 450 5 3)))
    (check robot-equal? (send d1 after-tick) (make-DAGR 345 453 5 3))
    (check robot-equal? (send d1 after-tick-while-paused) d1)
    (check robot-equal? (send d1 after-key-event "d") d1)
    (check robot-equal? (send d1 after-button-down 11 12) d1)
    (check robot-equal? (send d1 after-button-up 11 22) d1)
    (check robot-equal? (send d1 after-drag 33 44) d1))


  (local
    ((define d1 (make-SAGR 340 450 5 3)))
    (check robot-equal? (send d1 after-tick) (make-SAGR 345 453 5 3))
    (check robot-equal? (send d1 after-tick-while-paused) d1)
    (check robot-equal? (send d1 after-key-event "d") d1)
    (check robot-equal? (send d1 after-button-down 11 12) d1)
    (check robot-equal? (send d1 after-button-up 11 22) d1)
    (check robot-equal? (send d1 after-drag 33 44) d1))

  
  (local
    ((define SAGR1 (new SAGR% [x 50][y 80][t 0][vx -5][vy -5]))
     (define SAGR2 (new SAGR% [x 60][y 90][t 0][vx -5][vy -5]))
     (define SAGR3 (new SAGR% [x 50][y 80][t 0][vx -5][vy -5]))
     (define SAGR4 (new SAGR% [x 0][y 0][t 0][vx -5][vy -5]))
     (define SAGR5 (new SAGR% [x 220][y 0][t 0][vx -5][vy -5]))
     (define SAGR6 (new SAGR% [x 0][y 220][t 0][vx -5][vy -5]))
     (define SAGR7 (new SAGR% [x 220][y 80][t 0][vx -5][vy -5]))
     (define ball1 (new Goofball% [x 50] [y 75] [vx 5] [vy 3]
                        [is-stealthy? true] [selected? false]))
     (define ball2 (new Goofball% [x 55] [y 80] [vx 5] [vy 3]
                        [is-stealthy? true] [selected? false]))
     (define ball3 (new Goofball% [x 45] [y 80] [vx 5] [vy 3]
                        [is-stealthy? true] [selected? false]))
     (define ball4 (new Goofball% [x 50] [y 80] [vx 5] [vy 3]
                        [is-stealthy? true] [selected? false])))
    
    (check-equal? (send SAGR1 get-x) 50)
    (check-equal? (send SAGR1 get-y) 80)
    (check-equal? (send SAGR1 get-t) 0)
    (check-equal? (send SAGR1 get-vx) -5)
    (check-equal? (send SAGR1 get-vy) -5)
    
    (send SAGR1 after-tick)
    
    (check-equal? (send SAGR1 get-x) 45)
    (check-equal? (send SAGR1 get-y) 75)
    
    (send SAGR1 change-speed ball1)
    (check-equal? (send SAGR1 get-vx) 8)
    
    
    (send SAGR2 change-speed ball2)
    (check-equal? (send SAGR2 get-vx) -8)
    
    (send SAGR1 change-speed ball3)
    (check-equal? (send SAGR1 get-vx) 0)

    (send SAGR3 change-speed ball4)
    (check-equal? (send SAGR3 get-vx) 0)
    (check-equal? (send SAGR3 get-vy) 0)

    (send SAGR4 change-speed ball4)
    (check-equal? (send SAGR4 get-vx) 8)
    (check-equal? (send SAGR4 get-vy) 8)

    (send SAGR5 change-speed ball4)
    (check-equal? (send SAGR5 get-vx) -8)
    (check-equal? (send SAGR5 get-vy) 8)
    
    (send SAGR6 change-speed ball4)
    (check-equal? (send SAGR6 get-vx) 8)
    (check-equal? (send SAGR6 get-vy) -8)

    (send SAGR7 change-speed ball4)
    (check-equal? (send SAGR7 get-vx) -8)
    (check-equal? (send SAGR7 get-vy) 0)
    
    )

  (local
    ((define sg1 (make-stealth-goofball 10 13 1 1))
     (define sg2 (make-stealth-goofball 11 14 1 1))
     (define sg3 (make-stealth-goofball 790 13 1 1))
     (define sg4 (make-stealth-goofball 791 14 1 1))
     (define sg5 (make-stealth-goofball 13 10 1 1))
     (define sg6 (make-stealth-goofball 14 11 1 1))
     (define sg7 (make-stealth-goofball 10 587 1 1))
     (define sg8 (make-stealth-goofball 11 588 1 1))
     (define sg9 (make-stealth-goofball 10 591 1 1))
     (define sg10 (make-stealth-goofball 11 592 1 1)))
    (check goofball-equal? (send sg1 after-tick) sg2)
    (check goofball-equal? (send sg3 after-tick) sg4)
    (check goofball-equal? (send sg5 after-tick) sg6)
    (check goofball-equal? (send sg7 after-tick) sg8)
    (check goofball-equal? (send sg9 after-tick) sg10))

  (local
    ((define sg1 (make-stealth-goofball 787 10 1 1))
     (define sg2 (make-stealth-goofball 788 11 1 1))
     (define sg3 (make-stealth-goofball 790 587 1 1))
     (define sg4 (make-stealth-goofball 791 588 1 1))
     (define sg5 (make-stealth-goofball 787 590 1 1))
     (define sg6 (make-stealth-goofball 788 591 1 1)))
    (check goofball-equal? (send sg1 after-tick) sg2)
    (check goofball-equal? (send sg3 after-tick) sg4)
    (check goofball-equal? (send sg5 after-tick) sg6))





  (local
    ((define sg1 (make-stealth-goofball 10 100 -12 -11))
     (define sg2 (make-stealth-goofball 2 89 12 -11))
     (define sg3 (make-stealth-goofball 100 10 -11 -12))
     (define sg4 (make-stealth-goofball 89 2 -11 12))
     (define sg5 (make-stealth-goofball 790 100 12 -11))
     (define sg6 (make-stealth-goofball 798 89 -12 -11))
     (define sg7 (make-stealth-goofball 100 590 -11 12))
     (define sg8 (make-stealth-goofball 89 598 -11 -12)))
    (check goofball-equal? (send sg1 after-tick) sg2)
    (check goofball-equal? (send sg3 after-tick) sg4)
    (check goofball-equal? (send sg5 after-tick) sg6)
    (check goofball-equal? (send sg7 after-tick) sg8))
  
  
  (local
    ((define s1 (make-world empty empty)))
    (check-equal? (simulator-equal? s1 (initial-world)) true))

  (local
    ((define ball1 (make-stealth-goofball 50 75 2 3))
     (define log1 (make-world (list (make-stealth-goofball 50 75 2 3)
                      (make-stealth-goofball 50 75 2 3)
                      (make-stealth-goofball 40 40 0 0)) empty))
     (define log2 (make-stealth-goofball 40 40 0 0)))
    (check-equal? (send ball1 get-x) 50)
    (check-equal? (list-equal?
                   (send log1 widgets-coincide (send log1 goofballs))
                   (list log2) 1) true))

  (local
    ((define s1 (make-world empty empty))
     (define sg1 (make-stealth-goofball 50 75 5 3))
     (define sagr (make-SAGR 790 497 -10 -3))
     (define dagr (make-DAGR 795 497 -5 -3)))
    (check-equal?
     (simulator-equal?
      (world-after-key-event
       (world-after-key-event
        (world-after-key-event s1 "n") "s") "r")
      (make-world (list sg1) (list dagr sagr))) true))

  (local
    ((define sg1 (new Goofball% [x 50][y 75][vx 5][vy 3][is-stealthy? true]
                      [selected? true]))
     (define s1 (make-world (list sg1) empty))
     (define s2 (new SimulatorState% [launch-y 500][log (list sg1)][lor empty]
                     [paused? false][explosions empty])))
    (check-equal?
     (simulator-equal?
      (world-after-key-event
       (world-after-key-event
        (world-after-key-event
         (world-after-key-event
          (world-after-key-event
       (world-after-key-event
        (world-after-key-event s1 "right") "left") "up") "down") "u") "d") " ")
      s2) true))

  (local
    ((define sg1 (new Goofball% [x 50][y 75][vx 5][vy 3][is-stealthy? true]
                      [selected? false])))
    (check goofball-equal? (send sg1 after-key-event "right") sg1)
    (check goofball-equal? (send sg1 after-drag 200 300) sg1)
    (check goofball-equal? (send sg1 after-button-up 500 450) sg1))

  (local
    ((define s1 (new SimulatorState% [launch-y 600][log empty][lor empty]
                     [paused? false][explosions empty])))
    (check-equal?
     (simulator-equal? (world-after-key-event s1 "d") s1) true))

  (local
    ((define s1 (new SimulatorState% [launch-y 0][log empty][lor empty]
                     [paused? false][explosions empty])))
    (check-equal?
     (simulator-equal? (world-after-key-event s1 "u") s1) true))


  (local
    ((define sg1 (new Goofball% [x 50] [y 75] [vx 5] [vy 3]
                      [is-stealthy? true] [selected? true]))
     (define s1 (make-world (list sg1) empty)))
    (check goofball-equal? (send sg1 after-tick)
           sg1))


   (local
    ((define sg1 (new Goofball% [x 50] [y 75] [vx 5] [vy 3]
                      [is-stealthy? true] [selected? false]))
     (define sg2 (new Goofball% [x 50] [y 75] [vx 5] [vy 3]
                      [is-stealthy? true] [selected? true]
                      [saved-mx (- 55 50)] [saved-my (- 80 75)]))
     (define s1 (make-world (list sg1) empty))
     (define s2 (make-world (list sg2) empty)))
    (check simulator-equal? (world-after-mouse-event s1 55 80 "button-down")
           s2))

   (local
    ((define sg1 (new Goofball% [x 50] [y 75] [vx 5] [vy 3]
                      [is-stealthy? false] [selected? false]))
     (define sg2 (new Goofball% [x 50] [y 75] [vx 5] [vy 3]
                      [is-stealthy? false] [selected? true]
                      [saved-mx (- 55 50)] [saved-my (- 80 75)]))
     (define s1 (make-world (list sg1) empty))
     (define s2 (make-world (list sg2) empty)))
    (check simulator-equal? (world-after-mouse-event s1 55 80 "button-down")
           s2))


  (local
    ((define sg1 (new Goofball% [x 50] [y 75] [vx 5] [vy 3]
                      [is-stealthy? true] [selected? true]
                      [saved-mx 5] [saved-my 5]))
     (define sg2 (new Goofball% [x 70] [y 85] [vx 5] [vy 3]
                      [is-stealthy? true] [selected? true]
                      [saved-mx 5] [saved-my 5]))
     (define s1 (make-world (list sg1) empty))
     (define s2 (make-world (list sg2) empty)))
    (check simulator-equal? (world-after-mouse-event s1 75 90 "drag")
           s2))

  (local
    ((define sg1 (new Goofball% [x 50] [y 75] [vx 5] [vy 3]
                      [is-stealthy? true] [selected? true]
                      [saved-mx 5] [saved-my 5]))
     (define sg2 (new Goofball% [x 70] [y 85] [vx 5] [vy 3]
                      [is-stealthy? true] [selected? true]
                      [saved-mx 5] [saved-my 5]))
     (define s1 (make-world (list sg1) empty))
     (define s2 (make-world (list sg1) empty)))
    (check simulator-equal? (world-after-mouse-event s1 400 400 "drag") 
           s2))


  (local
    ((define sg1 (new Goofball% [x 70] [y 85] [vx 5] [vy 3]
                      [is-stealthy? true] [selected? true]
                      [saved-mx 5] [saved-my 5]))
     (define sg2 (new Goofball% [x 70] [y 85] [vx 5] [vy 3]
                      [is-stealthy? true] [selected? false]
                      [saved-mx 0] [saved-my 0]))
     (define s1 (make-world (list sg1) empty))
     (define s2 (make-world (list sg2) empty)))
    (check simulator-equal? (world-after-mouse-event s1 75 90 "button-up")
           s2))

  (local
    ((define sg1 (new Goofball% [x 50] [y 75] [vx 5] [vy 3]
                      [is-stealthy? true] [selected? false]))
     (define sg2 (new Goofball% [x 50] [y 75] [vx 5] [vy 3]
                      [is-stealthy? true] [selected? true]
                      [saved-mx (- 55 50)] [saved-my (- 80 75)]))
     (define s1 (make-world (list sg1) empty))
     (define s2 (make-world (list sg2) empty)))
    (check simulator-equal? (world-after-mouse-event s1 500 400 "button-down")
           s1))

  


  (local
    ((define sg1 (make-stealth-goofball 77 67 3 -1))
     (define sg2 (make-stealth-goofball 77 67 10 -1))
     
     (define sg4 (make-stealth-goofball 234 413 8 1))
     (define sg5 (make-stealth-goofball 240 415 2 -1))
     (define sg3 (make-stealth-goofball 500 10 -14 16))
     (define s1 (make-world (list sg1 sg2 sg3 sg4 sg5) empty)))
    (check-equal?
     (list-equal?
      (send s1 widgets-coincide (send s1 goofballs))
            (list sg3) 1) true))


  (local
    ((define sg1 (make-stealth-goofball 77 67 3 -1))
     (define sg2 (make-stealth-goofball 77 67 10 -1))
     
     (define sg4 (make-stealth-goofball 234 413 8 1))
     (define sg5 (make-stealth-goofball 240 415 2 -1))
     (define sg3 (make-stealth-goofball 500 10 -14 16))
     (define rg3 (make-round-goofball 500 10 -14 16))
     (define sag1 (make-SAGR 520 12 -6 19))
     (define s1 (make-world (list sg1 sg2 sg3 sg4 sg5) (list sag1))))
    (check-equal?
     (list-equal? (send (world-after-tick s1) goofballs)
                  (list rg3) 1) true))


  (local
    ((define sg1 (make-stealth-goofball 77 67 3 -1))
     (define sg2 (make-stealth-goofball 77 67 10 -1))
     
     (define sg4 (make-stealth-goofball 234 413 8 1))
     (define sg5 (make-stealth-goofball 240 415 2 -1))
     (define sg3 (make-stealth-goofball 500 10 -14 16))
     (define sag1 (make-SAGR 520 12 -6 19))
     (define s1 (make-world (list sg1 sg2 sg3 sg4 sg5) (list sag1))))
   (check-equal?
     (list-equal? (first (send s1 robots-overlap-goofballs))
                  (list sag1) 2) true))

  (local
    ((define sg1 (make-stealth-goofball 77 67 3 -1))
     (define sg2 (make-stealth-goofball 77 67 10 -1))
     
     (define sg4 (make-stealth-goofball 234 413 8 1))
     (define sg5 (make-stealth-goofball 240 415 2 -1))
     (define sg3 (make-stealth-goofball 500 10 -14 16))
     (define sag1 (make-SAGR 520 12 -6 19))
     (define s1 (make-world (list sg1 sg2 sg3 sg4 sg5) (list sag1))))
    (check-equal?
     (list-equal? (send (world-after-tick s1) robots)
                  empty 2) true))

  (local
    ((define sg1 (make-stealth-goofball 77 67 3 -1))
     (define sg2 (make-stealth-goofball 77 67 10 -1))
     
     (define sg4 (make-stealth-goofball 234 413 8 1))
     (define sg5 (make-stealth-goofball 240 415 2 -1))
     (define sg3 (make-stealth-goofball 500 10 -14 16))
     (define rg3 (make-round-goofball 500 10 -14 16))
     (define rg4 (make-round-goofball 394 193 -1 -1))
     (define sag1 (make-SAGR 520 12 -6 19))
     (define dag1 (make-DAGR 400 200 1 1))
     (define s1 (make-world (list sg1 sg2 sg3 sg4 sg5 rg4)
                            (list dag1 sag1)))
     (define s2 (make-world (list rg3) empty)))
    (check
     simulator-equal? (world-after-tick s1) s2))

  

  (local
    ((define rg1 (new Goofball% [x 10][y 10][vx -12][vy -12][selected? false]
                      [is-stealthy? false][radius 10]))
     (define rg2 (new Goofball% [x 2][y 2][vx 12][vy 12][selected? false]
                      [is-stealthy? false][radius 2]))
     (define rg3 (new Goofball% [x 10][y 590][vx -12][vy 12][selected? false]
                      [is-stealthy? false][radius 10]))
     (define rg4 (new Goofball% [x 2][y 598][vx 12][vy -12][selected? false]
                      [is-stealthy? false][radius 2]))
     (define rg5 (new Goofball% [x 790][y 590][vx 12][vy 12][selected? false]
                      [is-stealthy? false][radius 10]))
     (define rg6 (new Goofball% [x 798][y 598][vx -12][vy -12][selected? false]
                      [is-stealthy? false][radius 2]))
     (define rg7 (new Goofball% [x 790][y 10][vx 12][vy -12][selected? false]
                      [is-stealthy? false][radius 10]))
     (define rg8 (new Goofball% [x 798][y 2][vx -12][vy 12][selected? false]
                      [is-stealthy? false][radius 2]))
     
     (define s1 (new SimulatorState% [launch-y 500][log (list rg5 rg1)]
                     [lor empty]
                     [paused? false][explosions empty]))
     (define s2 (new SimulatorState% [launch-y 500][log (list rg6 rg2)]
                     [lor empty]
                     [paused? false][explosions empty]))
     (define s3 (new SimulatorState% [launch-y 500][log (list rg3 rg7)]
                     [lor empty]
                     [paused? false][explosions empty]))
     (define s4 (new SimulatorState% [launch-y 500][log (list rg4 rg8)]
                     [lor empty]
                     [paused? false][explosions empty])))
    (check
     simulator-equal? (world-after-tick s1) s2)
    (check
     simulator-equal? (world-after-tick s3) s4))

  (local
    ((define sg1 (make-stealth-goofball 77 67 3 -1))
     (define sg2 (make-stealth-goofball 77 67 10 -1))
     
     (define sg4 (make-stealth-goofball 234 413 8 1))
     (define sg5 (make-stealth-goofball 240 415 2 -1))
     (define sg3 (make-stealth-goofball 500 10 -14 16))
     (define rg3 (make-round-goofball 486 26 -14 16))
     (define sag1 (make-SAGR 520 12 -6 19))
     (define s1 (new SimulatorState% [launch-y 500]
                     [log (list sg1 sg2 sg3 sg4 sg5)][lor (list sag1)]
                     [paused? false][explosions empty]))
     (define s2 (new SimulatorState% [launch-y 500]
                     [log (list rg3)][lor empty]
                     [paused? false][explosions empty])))
    (check
     simulator-equal? (world-after-tick s1) s2)
    (check-equal? (send s2 get-explosions) empty))

  (local
    ((define exp1 (new Explosion% [x 400][y 200][counter 14]))
     (define exp2 (new Explosion% [x 400][y 200][counter 14])))
    (check
     explosions-equal? exp1 exp2))

  (local
    ((define sg1 (make-stealth-goofball 77 67 3 -1))
     (define rg3 (make-round-goofball 486 26 -14 16))
     (define sg4 (make-stealth-goofball 234 413 8 1))
     (define sag1 (make-SAGR 280 400 9 10))
     (define sag2 (make-SAGR 280 400 -8 8))
     (define s1 (make-world (list sg1 rg3 sg4) (list sag1))))
    (check-equal? (send (first (send s1 robot-speed-change (list sag1))) get-vx)
                  -8))

  (local
    ((define exp1 (new Explosion% [x 400][y 200][counter 14]))
     (define exp2 (new Explosion% [x 400][y 200][counter -1])))
    (check-equal? (send exp1 get-x) 400)
    (check-equal? (send exp1 get-y) 200)
    (check-equal? (send exp1 get-counter) 14)
    (check-equal? (send (send exp1 after-tick) get-x) 400)
    (check-equal? (send (send exp1 after-tick-while-paused) get-x) 400)
    (check-equal? (send (send exp1 after-drag) get-x) 400)
    (check-equal? (send (send exp1 after-button-down) get-x) 400)
    (check-equal? (send (send exp1 after-button-up) get-x) 400)
    (check-equal? (send (send exp1 after-key-event) get-x) 400)
    (check-equal? (send (send exp2 after-tick) get-x) 400)
    (check-equal? (send (send exp2 after-tick-while-paused) get-x) 400))

  


  )


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;





 