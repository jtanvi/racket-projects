;; The first three lines of this file were inserted by DrRacket. They record metadata
;; about the language level of this file in a form that our tools can easily process.
#reader(lib "htdp-beginner-reader.ss" "lang")((modname editor) (read-case-sensitive #t) (teachpacks ((lib "image.rkt" "teachpack" "2htdp"))) (htdp-settings #(#t constructor repeating-decimal #f #t none #f ((lib "image.rkt" "teachpack" "2htdp")) #f)))
(check-location "02" "editor.rkt")

(require rackunit)
(require "extras.rkt")
(require 2htdp/universe)

(provide
 make-editor
 editor-pre
 editor-post
 editor?
 edit)


;; DATA DEFINITIONS:

(define-struct editor [pre post])
;; An Editor is (make-editor String String)
;; INTERPRETATION: (make-editor pre post) means the text in the editor is
;;                 (string-append (editor-pre ed) (editor-post ed) with an imaginary cursor between s and t
;;                 'editor-pre' is the string on left side of the cursor
;;                 'editor-post' is the string on the right side of the cursor
;;                 'editor?' is the predicate for datatype Editor
;; DESTRUCTOR TEMPLATE:
;; editor-fn: Editor -> Editor
;; (define (editor-fn ed)
;;   (cond [(....(editor-pre ed)...(editor-post ed)...]
;;         [.............]
;;         [else (....(editor-pre ed)...(editor-post ed)...]
;; EXAMPLES:
;; (make-editor "mark" "steven")
;; (make-editor "ab" "dce")

(define Mark (make-editor "mark" "steven"))
(define Hello (make-editor "hello" ""))
(define Morning (make-editor "" "morning"))

;; A KeyEvent is one of: 
;; – a single-character string, i.e., a string of length 1
;; – "left"
;; – "right"
;; – "\t"
;; – "\b"
;; – "\r"
;; INTERPRETATION: The KeyEvent represents single key input from the keyboard like -
;;                 1String - a, s, d, e, f...
;;                 "\t" - Tab key
;;                 "left" - left arrow key
;;                 "right" - right arrow key ..and so on.



;; edit: Editor KeyEvent -> Editor
;; GIVEN: A data-type Editor 'ed' and a single-character KeyEvent 'ke'
;; RETURNS: Adds the KeyEvent 'ke' to the end of the pre field
;; of ed, unless ke = backspace("\b"), where it deletes the character immewdiately
;; to the left of cursor
;; EXAMPLES:
;; (edit (make-editor "hello" "world") "_") => (make-editor "hello " "world")
;; (edit (make-editor "functions" "design") 
;; TESTS:
(check-expect (edit Mark "\b") (make-editor "mar" "steven"))
(check-expect (edit Morning "left") (make-editor "" "morning"))
(check-expect (edit Morning "right") (make-editor "m" "orning"))
(check-expect (edit Hello "left") (make-editor "hell" "o"))
(check-expect (edit Mark "left") (make-editor "mar" "ksteven"))
(check-expect (edit Mark "\r") (make-editor "mark" "steven"))
(check-expect (edit Morning "G") (make-editor "G" "morning"))

;; DESIGN STRATEGY: Use template for Editor on ed.
;; FUNCTION DEFINITION:
(define (edit ed ke)
  (cond
    [(editor? ed) (cond
                  [(key=? ke "\b") (make-editor (string-remove-last (editor-pre ed)) (editor-post ed))]
                  [(key=? ke "left") (make-editor (string-remove-last (editor-pre ed)) (string-append (string-last (editor-pre ed)) (editor-post ed)))]
                  [(key=? ke "right") (make-editor (string-append (editor-pre ed) (string-first (editor-post ed))) (string-rest (editor-post ed)))]
                  [(or (key=? ke "\t") (key=? ke "\r")) (make-editor (editor-pre ed) (editor-post ed))]
                  [else (make-editor (string-append (editor-pre ed) ke) (editor-post ed))])]))


;; string-first: String -> String
;; GIVEN: A String
;; RETURNS: Extracts the first 1String from the given String
;; EXAMPLES:
;; (string-first "Grace") => "G"
;; (string-first "!apple") => "!"
;; TESTS:
(check-expect (string-first "Maurice") "M")
(check-expect (string-first "filler") "f")
;; DESIGN STRATEGY: Cases on Strings
;; FUNCTION DEFINITION:
(define (string-first a-s)
  (cond
    [(string=? a-s "") ""]
    [else (substring a-s 0 1)]))


;; string-remove-last: String -> String
;; GIVEN: A String
;; RETURNS: Deletes the last 1String from the given String
;; EXAMPLES:
;; (string-first "Grace") => "Grac"
;; (string-first "!apple") => "!appl"
;; TESTS:
(check-expect (string-remove-last "Maurice") "Mauric")
(check-expect (string-remove-last "filler") "fille")
(check-expect (string-remove-last "") "")
(check-expect (string-remove-last " ") "")
;; DESIGN STRATEGY: Cases on Strings
;; FUNCTION DEFINITION:
(define (string-remove-last a-s)
  (cond
    [(string=? a-s "") ""]
    [else (substring a-s 0 (- (los a-s) 1))]))


;; string-rest: String -> String
;; GIVEN: A String
;; RETURNS: Removes the first 1String from the given String and returns the rest
;; EXAMPLES:
;; (string-rest "terminator") => "erminator"
;; (string-first "!apple") => "apple"
;; TESTS:
(check-expect (string-rest "Fetish") "etish")
(check-expect (string-rest "assignment") "ssignment")
;; DESIGN STRATEGY: Cases on Strings
;; FUNCTION DEFINITION:
(define (string-rest a-s)
  (cond
    [(string=? a-s "") ""]
    [else (substring a-s 1 (los a-s))]))


;; string-last: String -> String
;; GIVEN: A String
;; RETURNS: Extracts the last 1String from the given String
;; EXAMPLES:
;; (string-first "Persevere") => "e"
;; (string-first "grams") => "s"
;; TESTS:
(check-expect (string-last "psychic") "c")
(check-expect (string-last "filler") "r")
;; DESIGN STRATEGY: Cases on Strings
;; FUNCTION DEFINITION:
(define (string-last a-s)
  (cond
   [(string=? a-s "") ""]
   [else (substring a-s (- (los a-s) 1) (los a-s))]))


;; los: String -> Number
;; GIVEN: A String
;; RETURNS: The length of the String
;; EXAMPLES:
;; (string-first "Grace") => 5
;; (string-first "!apple") => 6
;; TESTS:
(check-expect (los "Destination#") 12)
(check-expect (los "filler") 6)
;; DESIGN STRATEGY: Combine Simpler Functions
;; FUNCTION DEFINITION:
(define (los a-s)
  (string-length a-s))



;; TESTS:
(check-expect (edit Hello "right") (make-editor "hello" ""))
(check-expect (edit Mark "right") (make-editor "marks" "teven"))
(check-expect (edit Hello "!") (make-editor "hello!" ""))
(check-expect (edit Morning "G") (make-editor "G" "morning"))
(check-expect (edit Mark "\t") (make-editor "mark" "steven"))




