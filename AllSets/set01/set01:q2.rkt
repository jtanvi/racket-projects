;; The first three lines of this file were inserted by DrRacket. They record metadata
;; about the language level of this file in a form that our tools can easily process.
#reader(lib "htdp-beginner-reader.ss" "lang")((modname set01:q2) (read-case-sensitive #t) (teachpacks ((lib "image.rkt" "teachpack" "2htdp"))) (htdp-settings #(#t constructor repeating-decimal #f #t none #f ((lib "image.rkt" "teachpack" "2htdp")) #f)))
(require rackunit)
(require "extras.rkt")

(provide string-last)

;; DATA DEFINITIONS: none

;; string-last: String -> 1String
;; GIVEN: A Non-Empty String
;; RETURNS: The last 1String from the given string
;;          (A 1String is a string of length 1)
;; EXAMPLES:
;; (string-last "Hope") => "Hop"
;; (string-last "Jell-0") => "0"     
;; DESIGN STRATEGY: Combine Simpler Functions

(define (string-last a-string)
  (substring a-string (- (string-length a-string) 1) (string-length a-string)))

;; TESTS:

(begin-for-test
  (check-equal? (string-last "Hide&Seek") "k"
                "The last 1String should be k")
  (check-equal? (string-last "Happy't") "t"
                "The last 1String should be t")
  (check-equal? (string-last "Happy\\") "\\"
                "The last 1String should be \\"))
